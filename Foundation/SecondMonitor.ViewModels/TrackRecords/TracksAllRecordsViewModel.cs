﻿namespace SecondMonitor.ViewModels.TrackRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using DataModel.TrackRecords;
    using Factory;

    public class TracksAllRecordsViewModel : AbstractViewModel<TrackRecord>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private List<ClassRecordsViewModel> _classRecords;
        private TimeSpan _overallBest;
        private ICommand _deleteAllRecordsForTrack;
        private string _trackName;

        public TracksAllRecordsViewModel(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            _classRecords = new List<ClassRecordsViewModel>();
        }

        public ICommand DeleteAllRecordsForTrack
        {
            get => _deleteAllRecordsForTrack;
            set => SetProperty(ref _deleteAllRecordsForTrack, value);
        }

        public string TrackName
        {
            get => _trackName;
            private set => SetProperty(ref _trackName, value);
        }

        public TimeSpan OverallBest
        {
            get => _overallBest;
            private set => SetProperty(ref _overallBest, value);
        }

        public List<ClassRecordsViewModel> ClassRecords
        {
            get => _classRecords;
            set => SetProperty(ref _classRecords, value);
        }

        protected override void ApplyModel(TrackRecord model)
        {
            if (model == null)
            {
                ClassRecords = new List<ClassRecordsViewModel>();
                TrackName = string.Empty;
                OverallBest = TimeSpan.Zero;
                return;
            }

            TrackName = model.TrackName;
            OverallBest = model.OverallRecord.GetOverAllBest()?.LapTime ?? TimeSpan.Zero;

            var newClassRecords = new List<ClassRecordsViewModel>();
            var recordsGroupedByCar = model.VehicleRecords.Where(x => x.GetOverAllBest() != null).GroupBy(x => x.GetOverAllBest().CarClass);
            foreach (IGrouping<string, NamedRecordSet> recordForClass in recordsGroupedByCar)
            {
                ClassRecordsViewModel classRecordsViewModel = _viewModelFactory.Create<ClassRecordsViewModel>();
                classRecordsViewModel.TrackName = TrackName;
                classRecordsViewModel.FromModel(recordForClass);
                newClassRecords.Add(classRecordsViewModel);
            }

            ClassRecords = newClassRecords.OrderBy(x => x.OverallBest).ToList();
        }

        public override TrackRecord SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}