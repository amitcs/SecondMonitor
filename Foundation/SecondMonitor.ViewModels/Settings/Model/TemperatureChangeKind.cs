﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public enum TemperatureChangeKind
    {
        Last5Minutes, FromSessionStart, FromStintStart
    }
}
