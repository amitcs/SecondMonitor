﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SecondMonitor.ViewModels.Settings.Model;

    [Serializable]
    public class WeatherInfoSettingsViewModel : AbstractViewModel<WeatherInfoSettings>
    {
        private string _practiceFirstAirTempInfo;
        private string _practiceSecondAirTempInfo;
        private string _practiceFirstTrackTempInfo;
        private string _practiceSecondTrackTempInfo;
        private string _raceFirstAirTempInfo;
        private string _raceSecondAirTempInfo;
        private string _raceFirstTrackTempInfo;
        private string _raceSecondTrackTempInfo;
        private TemperatureChangeKindMap _temperatureChangeKindMap;

        public WeatherInfoSettingsViewModel()
        {
            _temperatureChangeKindMap = new TemperatureChangeKindMap();
            AvailableTemperatureChangeValues = _temperatureChangeKindMap.GetAllHumanReadableValue().ToList();
        }

        public List<string> AvailableTemperatureChangeValues { get; }

        public string PracticeFirstAirTempInfo
        {
            get => _practiceFirstAirTempInfo;
            set
            {
                SetProperty(ref _practiceFirstAirTempInfo, value);
                PracticeFirstAirTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string PracticeSecondAirTempInfo
        {
            get => _practiceSecondAirTempInfo;
            set
            {
                SetProperty(ref _practiceSecondAirTempInfo, value);
                PracticeSecondAirTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string PracticeFirstTrackTempInfo
        {
            get => _practiceFirstTrackTempInfo;
            set
            {
                SetProperty(ref _practiceFirstTrackTempInfo, value); 
                PracticeFirstTrackTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string PracticeSecondTrackTempInfo
        {
            get => _practiceSecondTrackTempInfo;
            set
            {
                SetProperty(ref _practiceSecondTrackTempInfo, value);
                PracticeSecondTrackTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string RaceFirstAirTempInfo
        {
            get => _raceFirstAirTempInfo;
            set
            {
                SetProperty(ref _raceFirstAirTempInfo, value);
                RaceFirstAirTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string RaceSecondAirTempInfo
        {
            get => _raceSecondAirTempInfo;
            set
            {
                SetProperty(ref _raceSecondAirTempInfo, value);
                RaceSecondAirTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string RaceFirstTrackTempInfo
        {
            get => _raceFirstTrackTempInfo;
            set
            {
                SetProperty(ref _raceFirstTrackTempInfo, value);
                RaceFirstTrackTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public string RaceSecondTrackTempInfo
        {
            get => _raceSecondTrackTempInfo;
            set
            {
                SetProperty(ref _raceSecondTrackTempInfo, value);
                RaceSecondTrackTempInfoKind = _temperatureChangeKindMap.FromHumanReadable(value);
            }
        }

        public TemperatureChangeKind RaceFirstAirTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind RaceSecondAirTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind RaceFirstTrackTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind RaceSecondTrackTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind PracticeFirstAirTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind PracticeSecondAirTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind PracticeFirstTrackTempInfoKind
        {
            get;
            private set;
        }

        public TemperatureChangeKind PracticeSecondTrackTempInfoKind
        {
            get;
            private set;
        }

        protected override void ApplyModel(WeatherInfoSettings model)
        {
            PracticeFirstAirTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.PracticeFirstAirTempInfo);
            PracticeSecondAirTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.PracticeSecondAirTempInfo);
            PracticeFirstTrackTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.PracticeFirstTrackTempInfo);
            PracticeSecondTrackTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.PracticeSecondTrackTempInfo);

            RaceFirstAirTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.RaceFirstAirTempInfo);
            RaceSecondAirTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.RaceSecondAirTempInfo);
            RaceFirstTrackTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.RaceFirstTrackTempInfo);
            RaceSecondTrackTempInfo = _temperatureChangeKindMap.ToHumanReadable(model.RaceSecondTrackTempInfo);
        }

        public override WeatherInfoSettings SaveToNewModel()
        {
            return new WeatherInfoSettings()
            {
                PracticeFirstAirTempInfo = _temperatureChangeKindMap.FromHumanReadable(_practiceFirstAirTempInfo),
                PracticeSecondAirTempInfo = _temperatureChangeKindMap.FromHumanReadable(_practiceSecondAirTempInfo),
                PracticeFirstTrackTempInfo = _temperatureChangeKindMap.FromHumanReadable(_practiceFirstTrackTempInfo),
                PracticeSecondTrackTempInfo = _temperatureChangeKindMap.FromHumanReadable(_practiceSecondTrackTempInfo),

                RaceFirstAirTempInfo = _temperatureChangeKindMap.FromHumanReadable(_raceFirstAirTempInfo),
                RaceSecondAirTempInfo = _temperatureChangeKindMap.FromHumanReadable(_raceSecondAirTempInfo),
                RaceFirstTrackTempInfo = _temperatureChangeKindMap.FromHumanReadable(_raceFirstTrackTempInfo),
                RaceSecondTrackTempInfo = _temperatureChangeKindMap.FromHumanReadable(_raceSecondTrackTempInfo),
            };
        }
    }
}
