﻿namespace SecondMonitor.ViewModels.Settings.ViewModel.Migrations
{
    using Model;

    public interface ISettingMigration
    {
         void MigrateUp(DisplaySettings displaySettings);
    }
}