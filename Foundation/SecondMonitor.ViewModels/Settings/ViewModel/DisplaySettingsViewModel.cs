﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Input;
    using Contracts.Commands;
    using DataModel.BasicProperties;
    using DataModel.BasicProperties.FuelConsumption;
    using DataModel.BasicProperties.Units;
    using Factory;
    using Layouts;
    using Migrations;
    using Model;
    using Overrides;
    using PluginsSettings;

    public class DisplaySettingsViewModel : AbstractViewModel<DisplaySettings>
    {
        private readonly IViewModelFactory _viewModelFactory;

        private readonly PitStopTimeDisplayMap _pitStopTimeDisplayMap;

        private IUomOverride _activeUomOverride;
        private VelocityUnits _velocityUnits;
        private TemperatureUnits _temperatureUnits;
        private PressureUnits _pressureUnits;
        private VolumeUnits _volumeUnits;
        private FuelCalculationScope _fuelCalculationScope;
        private int _paceLaps;
        private int _refreshRate;
        private bool _scrollToPlayer;
        private ReportingSettingsViewModel _reportingSettingsView;
        private bool _animateDriverPosition;
        private bool _animateDeltaTimes;

        private MapDisplaySettingsViewModel _mapDisplaySettingsViewModel;
        private TelemetrySettingsViewModel _telemetrySettingsViewModel;
        private MultiClassDisplayKind _multiClassDisplayKind;
        private ForceUnits _forceUnits;
        private AngleUnits _angleUnits;
        private PowerUnits _powerUnits;
        private TorqueUnits _torqueUnits;
        private bool _isGapVisualizationEnabled;
        private double _minimalGapForVisualization;
        private double _gapHeightForOneSecond;
        private double _maximumGapHeight;
        private RatingSettingsViewModel _ratingSettingsViewModel;
        private PitBoardSettingsViewModel _pitBoardSettingsViewModel;
        private TrackRecordsSettingsViewModel _trackRecordsSettingsViewModel;
        private bool _enablePedalInformation;
        private bool _enableTemperatureInformation;
        private bool _enableNonTemperatureInformation;
        private bool _enableCamberVisualization;
        private string _customResourcesPath;
        private int _driversUpdatedPerTick;
        private bool _isHwAccelerationEnabled;
        private LayoutSettingsViewModel _layoutSettingsViewModel;
        private IViewModel _layoutEditorViewModel;
        private bool _isForceSingleClassEnabled;
        private string _selectedPitStopDisplayMode;
        private bool _showPitStopTimeRelative;

        private int _maximumExtraFuel;
        private int _minimumExtraFuel;
        private int _racesToTrainFuel;
        private int _extraRaceLaps;
        private int _extraRaceMinutes;
        private int _extraContingencyFuel;
        private bool _overrideToPsiInAcc;
        private bool _paceContainsLapsToCatchEstimation;
        private int _gapClosingMaximumPositionDifference;
        private double _gapClosingOvertakeTimeLost;
        private int _gapClosingExtraLaps;
        private bool _gapComputeOnlyOnLapStart;
        private PitStopTimeDisplayKind _pitStopTimeDisplayKind;
        private WindowLocationSetting _windowLocationSetting;
        private WindowLocationSetting _mapWindowLocationSettings;
        private Func<LayoutDescription> _defaultLayoutCallback;
        private double _gapClosingInitialTime;
        private int _defaultRaceLaps = 0;
        private int _defaultRaceMinutes = 0;
        private bool _isRecentLowestSpeedEnabled;
        private bool _isRecentHighestSpeedEnabled;
        private int _lowestHighestSpeedTimeout;
        private int _hybridModeRelativeInFront;
        private int _hybridModeRelativeInBehind;
        private bool _isHybridModeGapEnabled;
        private int _hybridModeGapHeight;

        public DisplaySettingsViewModel(IViewModelFactory viewModelFactory, List<IUomOverride> uomOverrides)
        {
            _viewModelFactory = viewModelFactory;
            uomOverrides.ForEach(x => x.ActivationStateChanged += OnUomOverridesActivationStateChanged);
            SelectCustomResourceFileCommand = new RelayCommand(SelectCustomResourceFile);
            SessionOptionsViewModel = viewModelFactory.Create<SessionsOptionsViewModel>();
            PluginsConfigurationViewModel = viewModelFactory.Create<IPluginsConfigurationViewModel>();
            PitEstimationSettingsViewModel = viewModelFactory.Create<PitEstimationSettingsViewModel>();
            _pitStopTimeDisplayMap = new PitStopTimeDisplayMap();
            AllowedPitStopDisplayModes = _pitStopTimeDisplayMap.GetAllHumanReadableValue().ToArray();
            PracticeFuelSettings = viewModelFactory.Create<SessionFuelCalculationSettingsViewModel>();
            QualificationFuelSettings = viewModelFactory.Create<SessionFuelCalculationSettingsViewModel>();
            DashboardSettingsViewModel = viewModelFactory.Create<DashboardSettingsViewModel>();
            WeatherInfoSettings = viewModelFactory.Create<WeatherInfoSettingsViewModel>();
        }

        public WeatherInfoSettingsViewModel WeatherInfoSettings { get; }

        public SessionFuelCalculationSettingsViewModel PracticeFuelSettings { get; }

        public SessionFuelCalculationSettingsViewModel QualificationFuelSettings { get; }

        public DashboardSettingsViewModel DashboardSettingsViewModel { get; }

        public ICommand OpenLogDirectoryCommand => new RelayCommand(OpenLogDirectory);

        public PitEstimationSettingsViewModel PitEstimationSettingsViewModel { get; }

        public TelemetrySettingsViewModel TelemetrySettingsViewModel
        {
            get => _telemetrySettingsViewModel;
            set
            {
                SetProperty(ref _telemetrySettingsViewModel, value);
                NotifyPropertyChanged();
            }
        }

        public TemperatureUnits TemperatureUnits
        {
            get => _temperatureUnits;
            set
            {
                SetProperty(ref _temperatureUnits, value);
                NotifyPropertyChanged();
            }
        }

        public string[] AllowedPitStopDisplayModes { get; }

        public string SelectedPitStopDisplayMode
        {
            get => _selectedPitStopDisplayMode;
            set
            {
                SetProperty(ref _selectedPitStopDisplayMode, value);
                PitStopTimeDisplayKind = _pitStopTimeDisplayMap.FromHumanReadable(value);
            }
        }

        public PitStopTimeDisplayKind PitStopTimeDisplayKind
        {
            get => _pitStopTimeDisplayKind;
            private set => SetProperty(ref _pitStopTimeDisplayKind, value);
        }

        public MultiClassDisplayKind MultiClassDisplayKind
        {
            get => _multiClassDisplayKind;
            set
            {
                SetProperty(ref _multiClassDisplayKind, value);
                NotifyPropertyChanged();
            }
        }

        public bool OverrideToPsiInAcc
        {
            get => _overrideToPsiInAcc;
            set => SetProperty(ref _overrideToPsiInAcc, value);
        }

        public PressureUnits DisplayPressureUnits => _activeUomOverride?.GetPressureUnits(this, _pressureUnits) ?? _pressureUnits;

        public bool IsRecentLowestSpeedEnabled
        {
            get => _isRecentLowestSpeedEnabled;
            set => SetProperty(ref _isRecentLowestSpeedEnabled, value);
        }

        public bool IsRecentHighestSpeedEnabled
        {
            get => _isRecentHighestSpeedEnabled;
            set => SetProperty(ref _isRecentHighestSpeedEnabled, value);
        }

        public int LowestHighestSpeedTimeout
        {
            get => _lowestHighestSpeedTimeout;
            set => SetProperty(ref _lowestHighestSpeedTimeout, value);
        }

        public PressureUnits PressureUnits
        {
            get => _pressureUnits;
            set
            {
                SetProperty(ref _pressureUnits, value);
                NotifyPropertyChanged();
            }
        }

        public VolumeUnits VolumeUnits
        {
            get => _volumeUnits;
            set
            {
                SetProperty(ref _volumeUnits, value);
                NotifyPropertyChanged();
            }
        }

        public VelocityUnits VelocityUnits
        {
            get => _velocityUnits;
            set
            {
                SetProperty(ref _velocityUnits, value);
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(DistanceUnits));
                NotifyPropertyChanged(nameof(FuelPerDistanceUnits));
            }
        }

        public ForceUnits ForceUnits
        {
            get => _forceUnits;
            set => SetProperty(ref _forceUnits, value);
        }

        public AngleUnits AngleUnits
        {
            get => _angleUnits;
            set => SetProperty(ref _angleUnits, value);
        }

        public TorqueUnits TorqueUnits
        {
            get => _torqueUnits;
            set => SetProperty(ref _torqueUnits, value);
        }

        public PowerUnits PowerUnits
        {
            get => _powerUnits;
            set => SetProperty(ref _powerUnits, value);
        }

        public FuelCalculationScope FuelCalculationScope
        {
            get => _fuelCalculationScope;
            set
            {
                SetProperty(ref _fuelCalculationScope, value);
                NotifyPropertyChanged();
            }
        }

        public SessionsOptionsViewModel SessionOptionsViewModel { get; }

        public bool EnablePedalInformation
        {
            get => _enablePedalInformation;
            set => SetProperty(ref _enablePedalInformation, value);
        }

        public bool EnableTemperatureInformation
        {
            get => _enableTemperatureInformation;
            set => SetProperty(ref _enableTemperatureInformation, value);
        }

        public bool EnableNonTemperatureInformation
        {
            get => _enableNonTemperatureInformation;
            set => SetProperty(ref _enableNonTemperatureInformation, value);
        }

        public DistanceUnits DistanceUnits
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Kilometers;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Miles;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Meters;
                    default:
                        return DistanceUnits.Kilometers;
                }
            }
        }

        public DistanceUnits DistanceUnitsSmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Meters;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Yards;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Meters;
                    default:
                        return DistanceUnits.Meters;
                }
            }
        }

        public DistanceUnits DistanceUnitsVerySmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Millimeter;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Inches;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Millimeter;
                    default:
                        return DistanceUnits.Millimeter;
                }
            }
        }

        public VelocityUnits VelocityUnitsVerySmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.Mph:
                        return VelocityUnits.InPerSecond;
                    case VelocityUnits.Ms:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.Fps:
                        return VelocityUnits.InPerSecond;
                    case VelocityUnits.CmPerSecond:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.InPerSecond:
                        return VelocityUnits.InPerSecond;
                    default:
                        return VelocityUnits.MMPerSecond;
                }
            }
        }

        public FuelPerDistanceUnits FuelPerDistanceUnits
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                    case VelocityUnits.Mph:
                        return FuelPerDistanceUnits.MilesPerGallon;
                    case VelocityUnits.Ms:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                    default:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                }
            }
        }

        public bool ShowPitStopTimeRelative
        {
            get => _showPitStopTimeRelative;
            set => SetProperty(ref _showPitStopTimeRelative, value);
        }

        public int PaceLaps
        {
            get => _paceLaps;
            set
            {
                SetProperty(ref _paceLaps, value);
                NotifyPropertyChanged();
            }
        }

        public bool PaceContainsLapsToCatchEstimation
        {
            get => _paceContainsLapsToCatchEstimation;
            set => SetProperty(ref _paceContainsLapsToCatchEstimation, value);
        }

        public int RefreshRate
        {
            get => _refreshRate;
            set
            {
                SetProperty(ref _refreshRate, value);
                NotifyPropertyChanged();
            }
        }

        public bool ScrollToPlayer
        {
            get => _scrollToPlayer;
            set
            {
                SetProperty(ref _scrollToPlayer, value);
                NotifyPropertyChanged();
            }
        }

        public bool AnimateDriversPosition
        {
            get => _animateDriverPosition;
            set
            {
                SetProperty(ref _animateDriverPosition, value);
                NotifyPropertyChanged();
            }
        }

        public bool AnimateDeltaTimes
        {
            get => _animateDeltaTimes;
            set
            {
                SetProperty(ref _animateDeltaTimes, value);
                NotifyPropertyChanged();
            }
        }

        public ReportingSettingsViewModel ReportingSettingsView
        {
            get => _reportingSettingsView;
            set
            {
                SetProperty(ref _reportingSettingsView, value);
                NotifyPropertyChanged();
            }
        }

        public MapDisplaySettingsViewModel MapDisplaySettingsViewModel
        {
            get => _mapDisplaySettingsViewModel;
            set
            {
                SetProperty(ref _mapDisplaySettingsViewModel, value);
                NotifyPropertyChanged();
            }
        }

        public bool IsGapVisualizationEnabled
        {
            get => _isGapVisualizationEnabled;
            set => SetProperty(ref _isGapVisualizationEnabled, value);
        }

        public double MinimalGapForVisualization
        {
            get => _minimalGapForVisualization;
            set => SetProperty(ref _minimalGapForVisualization, value);
        }

        public double GapHeightForOneSecond
        {
            get => _gapHeightForOneSecond;
            set => SetProperty(ref _gapHeightForOneSecond, value);
        }

        public double MaximumGapHeight
        {
            get => _maximumGapHeight;
            set => SetProperty(ref _maximumGapHeight, value);
        }

        public RatingSettingsViewModel RatingSettingsViewModel
        {
            get => _ratingSettingsViewModel;
            set => SetProperty(ref _ratingSettingsViewModel, value);
        }

        public PitBoardSettingsViewModel PitBoardSettingsViewModel
        {
            get => _pitBoardSettingsViewModel;
            set => SetProperty(ref _pitBoardSettingsViewModel, value);
        }

        public TrackRecordsSettingsViewModel TrackRecordsSettingsViewModel
        {
            get => _trackRecordsSettingsViewModel;
            set => SetProperty(ref _trackRecordsSettingsViewModel, value);
        }

        public WindowLocationSetting WindowLocationSetting
        {
            get => _windowLocationSetting;
            set => SetProperty(ref _windowLocationSetting, value);
        }

        public WindowLocationSetting MapWindowLocationSettings
        {
            get => _mapWindowLocationSettings;
            set => SetProperty(ref _mapWindowLocationSettings, value);
        }

        public bool EnableCamberVisualization
        {
            get => _enableCamberVisualization;
            set => SetProperty(ref _enableCamberVisualization, value);
        }

        public string CustomResourcesPath
        {
            get => _customResourcesPath;
            set => SetProperty(ref _customResourcesPath, value);
        }

        public int DriversUpdatedPerTick
        {
            get => _driversUpdatedPerTick;
            set => SetProperty(ref _driversUpdatedPerTick, value);
        }

        public bool IsHwAccelerationEnabled
        {
            get => _isHwAccelerationEnabled;
            set => SetProperty(ref _isHwAccelerationEnabled, value);
        }

        public LayoutSettingsViewModel LayoutSettingsViewModel
        {
            get => _layoutSettingsViewModel;
            set => SetProperty(ref _layoutSettingsViewModel, value);
        }

        public IViewModel LayoutEditorViewModel
        {
            get => _layoutEditorViewModel;
            set => SetProperty(ref _layoutEditorViewModel, value);
        }

        public Func<LayoutDescription> DefaultLayoutCallback
        {
            get => _defaultLayoutCallback;
            set => SetProperty(ref _defaultLayoutCallback, value);
        }

        public bool IsForceSingleClassEnabled
        {
            get => _isForceSingleClassEnabled;
            set => SetProperty(ref _isForceSingleClassEnabled, value);
        }

        public IPluginsConfigurationViewModel PluginsConfigurationViewModel { get; }

        public ICommand SelectCustomResourceFileCommand { get; }

        public int MaximumExtraFuel
        {
            get => _maximumExtraFuel;
            set => SetProperty(ref _maximumExtraFuel, value);
        }

        public int MinimumExtraFuel
        {
            get => _minimumExtraFuel;
            set => SetProperty(ref _minimumExtraFuel, value);
        }

        public int RacesToTrainFuel
        {
            get => _racesToTrainFuel;
            set => SetProperty(ref _racesToTrainFuel, value);
        }

        public int ExtraRaceLaps
        {
            get => _extraRaceLaps;
            set => SetProperty(ref _extraRaceLaps, value);
        }

        public int ExtraRaceMinutes
        {
            get => _extraRaceMinutes;
            set => SetProperty(ref _extraRaceMinutes, value);
        }

        public int ExtraContingencyFuel
        {
            get => _extraContingencyFuel;
            set => SetProperty(ref _extraContingencyFuel, value);
        }

        public int GapClosingMaximumPositionDifference
        {
            get => _gapClosingMaximumPositionDifference;
            set => SetProperty(ref _gapClosingMaximumPositionDifference, value);
        }

        public double GapClosingOvertakeTimeLost
        {
            get => _gapClosingOvertakeTimeLost;
            set => SetProperty(ref _gapClosingOvertakeTimeLost, value);
        }

        public int GapClosingExtraLaps
        {
            get => _gapClosingExtraLaps;
            set => SetProperty(ref _gapClosingExtraLaps, value);
        }

        public double GapClosingInitialTime
        {
            get => _gapClosingInitialTime;
            set => SetProperty(ref _gapClosingInitialTime, value);
        }

        public bool GapComputeOnlyOnLapStart
        {
            get => _gapComputeOnlyOnLapStart;
            set => SetProperty(ref _gapComputeOnlyOnLapStart, value);
        }

        public int DefaultRaceLaps
        {
            get => _defaultRaceLaps;
            set => SetProperty(ref _defaultRaceLaps, value);
        }

        public int DefaultRaceMinutes
        {
            get => _defaultRaceMinutes;
            set => SetProperty(ref _defaultRaceMinutes, value);
        }

        public int HybridModeRelativeInFront
        {
            get => _hybridModeRelativeInFront;
            set => SetProperty(ref _hybridModeRelativeInFront, value);
        }

        public int HybridModeRelativeInBehind
        {
            get => _hybridModeRelativeInBehind;
            set => SetProperty(ref _hybridModeRelativeInBehind, value);
        }

        public bool IsHybridModeGapEnabled
        {
            get => _isHybridModeGapEnabled;
            set => SetProperty(ref _isHybridModeGapEnabled, value);
        }

        public int HybridModeGapHeight
        {
            get => _hybridModeGapHeight;
            set => SetProperty(ref _hybridModeGapHeight, value);
        }

        protected override void ApplyModel(DisplaySettings settings)
        {
            TemperatureUnits = settings.TemperatureUnits;
            PressureUnits = settings.PressureUnits;
            OverrideToPsiInAcc = settings.OverrideToPsiInAcc;
            VolumeUnits = settings.VolumeUnits;
            VelocityUnits = settings.VelocityUnits;
            FuelCalculationScope = settings.FuelCalculationScope;
            PaceLaps = settings.PaceLaps;
            PaceContainsLapsToCatchEstimation = settings.PaceContainsLapsToCatchEstimation;
            RefreshRate = settings.RefreshRate;
            ScrollToPlayer = settings.ScrollToPlayer;
            AnimateDeltaTimes = settings.AnimateDeltaTimes;
            AnimateDriversPosition = settings.AnimateDriversPosition;
            MultiClassDisplayKind = settings.MultiClassDisplayKind;
            ForceUnits = settings.ForceUnits;
            AngleUnits = settings.AngleUnits;

            IsGapVisualizationEnabled = settings.IsGapVisualizationEnabled;
            MinimalGapForVisualization = settings.MinimalGapForVisualization;
            MaximumGapHeight = settings.MaximumGapHeight;
            GapHeightForOneSecond = settings.GapHeightForOneSecond;

            MapDisplaySettingsViewModel = new MapDisplaySettingsViewModel();
            MapDisplaySettingsViewModel.FromModel(settings.MapDisplaySettings);

            SessionOptionsViewModel.FromModel(settings);

            ReportingSettingsView = new ReportingSettingsViewModel();
            ReportingSettingsView.FromModel(settings.ReportingSettings);

            TelemetrySettingsViewModel = _viewModelFactory.CreateAndApply<TelemetrySettingsViewModel, TelemetrySettings>(settings.TelemetrySettings);
            WindowLocationSetting = settings.WindowLocationSetting;
            MapWindowLocationSettings = settings.MapWindowLocationSettings;

            RatingSettingsViewModel = new RatingSettingsViewModel();
            RatingSettingsViewModel.FromModel(settings.RatingSettings);

            PitBoardSettingsViewModel = new PitBoardSettingsViewModel();
            PitBoardSettingsViewModel.FromModel(settings.PitBoardSettings);

            TrackRecordsSettingsViewModel = new TrackRecordsSettingsViewModel();
            TrackRecordsSettingsViewModel.FromModel(settings.TrackRecordsSettings);

            PowerUnits = settings.PowerUnits;
            TorqueUnits = settings.TorqueUnits;

            EnablePedalInformation = settings.EnablePedalInformation;
            EnableNonTemperatureInformation = settings.EnableNonTemperatureInformation;
            EnableTemperatureInformation = settings.EnableTemperatureInformation;
            EnableCamberVisualization = settings.EnableCamberVisualization;
            CustomResourcesPath = settings.CustomResourcesPath;
            DriversUpdatedPerTick = settings.DriversUpdatedPerTick;
            IsHwAccelerationEnabled = settings.IsHwAccelerationEnabled;

            LayoutSettingsViewModel = new LayoutSettingsViewModel();
            LayoutSettingsViewModel.FromModel(settings.LayoutSettings);

            IsForceSingleClassEnabled = settings.IsForceSingeClassEnabled;
            SelectedPitStopDisplayMode = _pitStopTimeDisplayMap.ToHumanReadable(settings.PitStopTimeDisplayKind);
            ShowPitStopTimeRelative = settings.ShowPitStopTimeRelative;

            PitEstimationSettingsViewModel.FromModel(settings.PitEstimationSettings);

            MaximumExtraFuel = settings.MaximumExtraFuel;
            MinimumExtraFuel = settings.MinimumExtraFuel;
            RacesToTrainFuel = settings.RacesToTrainFuel;

            DefaultRaceLaps = settings.DefaultRaceLaps;
            DefaultRaceMinutes = settings.DefaultRaceMinutes;

            ExtraContingencyFuel = settings.ExtraContingencyFuel;
            ExtraRaceLaps = settings.ExtraRaceLaps;
            ExtraRaceMinutes = settings.ExtraRaceMinutes;
            GapClosingExtraLaps = settings.GapClosingExtraLaps;
            GapClosingOvertakeTimeLost = settings.GapClosingOvertakeTimeLost;
            GapClosingMaximumPositionDifference = settings.GapClosingMaximumPositionDifference;
            GapClosingInitialTime = settings.GapClosingInitialTime;
            GapComputeOnlyOnLapStart = settings.GapComputeOnlyOnLapStart;
            PracticeFuelSettings.FromModel(settings.PracticeFuelSettings);
            QualificationFuelSettings.FromModel(settings.QualificationFuelSettings);

            IsRecentLowestSpeedEnabled = settings.IsRecentLowestSpeedEnabled;
            IsRecentHighestSpeedEnabled = settings.IsRecentHighestSpeedEnabled;
            LowestHighestSpeedTimeout = settings.LowestHighestSpeedTimeout;

            HybridModeRelativeInFront = settings.HybridModeRelativeInFront;
            HybridModeRelativeInBehind = settings.HybridModeRelativeInBehind;
            HybridModeGapHeight = settings.HybridModeGapHeight;
            IsHybridModeGapEnabled = settings.IsHybridModeGapEnabled;
            DashboardSettingsViewModel.FromModel(settings.DashboardSettings);
            WeatherInfoSettings.FromModel(settings.WeatherInfoSettings);
        }

        public override DisplaySettings SaveToNewModel()
        {
            var displaySettings = new DisplaySettings()
            {
                TemperatureUnits = TemperatureUnits,
                PressureUnits = PressureUnits,
                OverrideToPsiInAcc = OverrideToPsiInAcc,
                VolumeUnits = VolumeUnits,
                VelocityUnits = VelocityUnits,
                FuelCalculationScope = FuelCalculationScope,
                PaceLaps = PaceLaps,
                PaceContainsLapsToCatchEstimation = PaceContainsLapsToCatchEstimation,
                RefreshRate = RefreshRate,
                ScrollToPlayer = ScrollToPlayer,
                ReportingSettings = ReportingSettingsView.ToModel(),
                AnimateDriversPosition = AnimateDriversPosition,
                AnimateDeltaTimes = AnimateDeltaTimes,
                MapDisplaySettings = MapDisplaySettingsViewModel.SaveToNewModel(),
                TelemetrySettings = TelemetrySettingsViewModel.SaveToNewModel(),
                MultiClassDisplayKind = MultiClassDisplayKind,
                ForceUnits = ForceUnits,
                AngleUnits = AngleUnits,
                IsGapVisualizationEnabled = IsGapVisualizationEnabled,
                MinimalGapForVisualization = MinimalGapForVisualization,
                MaximumGapHeight = MaximumGapHeight,
                GapHeightForOneSecond = GapHeightForOneSecond,
                WindowLocationSetting = WindowLocationSetting,
                MapWindowLocationSettings = MapWindowLocationSettings,
                RatingSettings = RatingSettingsViewModel.SaveToNewModel(),
                PitBoardSettings = PitBoardSettingsViewModel.SaveToNewModel(),
                TrackRecordsSettings = TrackRecordsSettingsViewModel.SaveToNewModel(),
                PowerUnits = PowerUnits,
                TorqueUnits = TorqueUnits,
                EnablePedalInformation = EnablePedalInformation,
                EnableTemperatureInformation = EnableTemperatureInformation,
                EnableNonTemperatureInformation = EnableNonTemperatureInformation,
                EnableCamberVisualization = EnableCamberVisualization,
                CustomResourcesPath = CustomResourcesPath,
                DriversUpdatedPerTick = DriversUpdatedPerTick,
                IsHwAccelerationEnabled = IsHwAccelerationEnabled,
                LayoutSettings = LayoutSettingsViewModel.SaveToNewModel(),
                IsForceSingeClassEnabled = IsForceSingleClassEnabled,
                PitStopTimeDisplayKind = _pitStopTimeDisplayMap.FromHumanReadable(SelectedPitStopDisplayMode),
                ShowPitStopTimeRelative = ShowPitStopTimeRelative,
                PitEstimationSettings = PitEstimationSettingsViewModel.SaveToNewModel(),
                MaximumExtraFuel = MaximumExtraFuel,
                MinimumExtraFuel = MinimumExtraFuel,
                RacesToTrainFuel = RacesToTrainFuel,
                ExtraContingencyFuel = ExtraContingencyFuel,
                ExtraRaceLaps = ExtraRaceLaps,
                ExtraRaceMinutes = ExtraRaceMinutes,
                DefaultRaceLaps = DefaultRaceLaps,
                DefaultRaceMinutes = DefaultRaceMinutes,
                GapClosingExtraLaps = GapClosingExtraLaps,
                GapClosingMaximumPositionDifference = GapClosingMaximumPositionDifference,
                GapClosingOvertakeTimeLost = GapClosingOvertakeTimeLost,
                GapClosingInitialTime = GapClosingInitialTime,
                PracticeFuelSettings = PracticeFuelSettings.SaveToNewModel(),
                QualificationFuelSettings = QualificationFuelSettings.SaveToNewModel(),
                IsRecentLowestSpeedEnabled = IsRecentLowestSpeedEnabled,
                IsRecentHighestSpeedEnabled = IsRecentHighestSpeedEnabled,
                LowestHighestSpeedTimeout = LowestHighestSpeedTimeout,
                HybridModeRelativeInFront = HybridModeRelativeInFront,
                HybridModeRelativeInBehind = HybridModeRelativeInBehind,
                HybridModeGapHeight = HybridModeGapHeight,
                IsHybridModeGapEnabled = IsHybridModeGapEnabled,
                DashboardSettings = DashboardSettingsViewModel.SaveToNewModel(),
                GapComputeOnlyOnLapStart = GapComputeOnlyOnLapStart,
                WeatherInfoSettings = WeatherInfoSettings.SaveToNewModel(),
            };

            SessionOptionsViewModel.ApplyToModel(displaySettings);
            return displaySettings;
        }

        private void SelectCustomResourceFile()
        {
            using (OpenFileDialog fbd = new OpenFileDialog())
            {
                fbd.DefaultExt = ".xaml";
                fbd.Filter = @"xaml Files (*.xaml)|*.xaml";
                fbd.CheckFileExists = true;
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    CustomResourcesPath = fbd.FileName;
                }
            }
        }

        private void OnUomOverridesActivationStateChanged(object sender, ActivationStateEventArgs e)
        {
            if (sender is not IUomOverride senderUomOverride)
            {
                return;
            }

            if (e.IsActive)
            {
                _activeUomOverride = senderUomOverride;
                NotifyPropertyChanged(string.Empty);
                return;
            }

            if (senderUomOverride != _activeUomOverride)
            {
                return;
            }

            _activeUomOverride = null;
            NotifyPropertyChanged(string.Empty);
        }

        private void OpenLogDirectory()
        {
            string reportDirectory = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "SecondMonitor");
            Task.Run(
                () => { Process.Start(reportDirectory); });
        }

        public LayoutDescription GetDefaultLayout()
        {
            if (DefaultLayoutCallback == null)
            {
                throw new InvalidOperationException("Default Layout callback not initialized.");
            }

            return DefaultLayoutCallback();
        }

        public UnitsCollection GetUnitsCollection()
        {
            return new UnitsCollection()
            {
                DistanceUnitsVerySmall = DistanceUnitsVerySmall,
                DistanceUnits = DistanceUnitsSmall,
                VelocityUnits = VelocityUnits,
                VelocityUnitsSmall = VelocityUnitsVerySmall,
                TemperatureUnits = TemperatureUnits,
                PressureUnits = DisplayPressureUnits,
                AngleUnits = AngleUnits,
                ForceUnits = ForceUnits,
            };
        }
    }
}