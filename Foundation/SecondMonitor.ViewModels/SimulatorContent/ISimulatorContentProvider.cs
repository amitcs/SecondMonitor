﻿namespace SecondMonitor.ViewModels.SimulatorContent
{
    using System.Collections.Generic;
    using DataModel.SimulatorContent;

    public interface ISimulatorContentProvider
    {
        IReadOnlyList<string> GetSimulatorsWithContent();
        IReadOnlyList<CarClass> GetAllCarClassesForSimulator(string simulatorName);
        IReadOnlyList<Track> GetAllTracksForSimulator(string simulatorName);
        IReadOnlyList<Car> GetAllCarsForSimulator(string simulatorName);
        IReadOnlyList<Car> GetAllCarsForSimulator(string simulatorName, string carClass);
        Track GetRandomTrack(string simulatorName);
        CarClass GetRandomClass(string simulatorName);
    }
}