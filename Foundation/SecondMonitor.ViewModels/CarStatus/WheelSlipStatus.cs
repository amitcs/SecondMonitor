﻿namespace SecondMonitor.ViewModels.CarStatus
{
    public enum WheelSlipStatus
    {
        NotSlipping,
        IsSlipping,
        WasSlipping,
    }
}