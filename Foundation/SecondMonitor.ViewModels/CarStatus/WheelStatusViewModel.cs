﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using System;
    using System.ComponentModel;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using FuelStatus;
    using Settings;

    public class WheelStatusViewModel : AbstractViewModel
    {
        private static readonly Temperature TemperatureTolerance = Temperature.FromCelsius(0.5);
        private static readonly Pressure PressureTolerance = Pressure.FromKiloPascals(0.1);
        private readonly ISettingsProvider _settingsProvider;
        private readonly TyreLifeTimeMonitor _tyreLifeTimeMonitor;

        private double _wearAtStintEnd;
        private string _idealPressure;
        private double _wearAtRaceEnd;
        private bool _isLeftWheel;

        private double _wheelCamber;
        private bool _isCamberVisible;
        private double _brakeCondition;
        private int _lapsUntilHeavyWear;
        private string _tyreCompound;
        private WheelSlipStatus _slipStatus;
        private TimeSpan _slipEndSessionTime;
        private bool _isTyreDetached;
        private TemperatureUnits _temperatureUnits;
        private PressureUnits _pressureUnits;

        private double _tyreCondition;
        private double _tyreNoWearWearLimit;
        private double _tyreMildWearLimit;
        private double _tyreHeavyWearLimit;

        private OptimalQuantity<Temperature> _tyreCoreTemperature;
        private OptimalQuantity<Temperature> _tyreLeftTemperature;
        private OptimalQuantity<Temperature> _tyreCenterTemperature;
        private OptimalQuantity<Temperature> _brakeTemperature;
        private OptimalQuantity<Pressure> _tyrePressure;
        private OptimalQuantity<Temperature> _tyreRightTemperature;

        public WheelStatusViewModel(bool isLeft, ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            IsLeftWheel = isLeft;
        }

        public WheelStatusViewModel(bool isLeft, SessionRemainingCalculator sessionRemainingCalculator, ISettingsProvider settingsProvider, IPaceProvider paceProvider, IFuelPredictionProvider fuelPredictionProvider) : this(isLeft, settingsProvider)
        {
            _tyreLifeTimeMonitor = new TyreLifeTimeMonitor(paceProvider, sessionRemainingCalculator, fuelPredictionProvider);
            ApplyDisplaySettings();
            settingsProvider.DisplaySettingsViewModel.PropertyChanged += DisplaySettingsViewModelOnPropertyChanged;
        }

        public double WheelCamber
        {
            get => _wheelCamber;
            set => SetProperty(ref _wheelCamber, value);
        }

        public bool IsCamberVisible
        {
            get => _isCamberVisible;
            set => SetProperty(ref _isCamberVisible, value);
        }

        public double BrakeCondition
        {
            get => _brakeCondition;
            set => SetProperty(ref _brakeCondition, value);
        }

        public int LapsUntilHeavyWear
        {
            get => _lapsUntilHeavyWear;
            set => SetProperty(ref _lapsUntilHeavyWear, value);
        }

        public string TyreCompound
        {
            get => _tyreCompound;
            set => SetProperty(ref _tyreCompound, value);
        }

        public WheelSlipStatus SlipStatus
        {
            get => _slipStatus;
            set => SetProperty(ref _slipStatus, value);
        }

        public bool IsTyreDetached
        {
            get => _isTyreDetached;
            set => SetProperty(ref _isTyreDetached, value);
        }

        public TemperatureUnits TemperatureUnits
        {
            get => _temperatureUnits;
            set => SetProperty(ref _temperatureUnits, value);
        }

        public PressureUnits PressureUnits
        {
            get => _pressureUnits;
            set => SetProperty(ref _pressureUnits, value);
        }

        public double TyreCondition
        {
            get => _tyreCondition;
            set => SetProperty(ref _tyreCondition, value);
        }

        public double TyreNoWearWearLimit
        {
            get => _tyreNoWearWearLimit;
            set => SetProperty(ref _tyreNoWearWearLimit, value);
        }

        public double TyreMildWearLimit
        {
            get => _tyreMildWearLimit;
            set => SetProperty(ref _tyreMildWearLimit, value);
        }

        public double TyreHeavyWearLimit
        {
            get => _tyreHeavyWearLimit;
            set => SetProperty(ref _tyreHeavyWearLimit, value);
        }

        public OptimalQuantity<Temperature> TyreCoreTemperature
        {
            get => _tyreCoreTemperature;
            set => SetProperty(ref _tyreCoreTemperature, value);
        }

        public OptimalQuantity<Temperature> TyreLeftTemperature
        {
            get => _tyreLeftTemperature;
            set => SetProperty(ref _tyreLeftTemperature, value);
        }

        public OptimalQuantity<Temperature> TyreCenterTemperature
        {
            get => _tyreCenterTemperature;
            set => SetProperty(ref _tyreCenterTemperature, value);
        }

        public OptimalQuantity<Temperature> TyreRightTemperature
        {
            get => _tyreRightTemperature;
            set => SetProperty(ref _tyreRightTemperature, value);
        }

        public OptimalQuantity<Temperature> BrakeTemperature
        {
            get => _brakeTemperature;
            set => SetProperty(ref _brakeTemperature, value);
        }

        public OptimalQuantity<Pressure> TyrePressure
        {
            get => _tyrePressure;
            set => SetProperty(ref _tyrePressure, value);
        }

        public bool IsLeftWheel
        {
            get => _isLeftWheel;
            set => SetProperty(ref _isLeftWheel, value);
        }

        public double WearAtRaceEnd
        {
            get => _wearAtRaceEnd;
            set => SetProperty(ref _wearAtRaceEnd, value);
        }

        public double WearAtStintEnd
        {
            get => _wearAtStintEnd;
            set => SetProperty(ref _wearAtStintEnd, value);
        }

        public string IdealPressure
        {
            get => _idealPressure;
            set => SetProperty(ref _idealPressure, value);
        }

        public void ApplyWheelCondition(WheelInfo wheelInfo)
        {
            TyreCondition = wheelInfo.Detached ? 0.1 : Math.Round(100 * (1 - wheelInfo.TyreWear.ActualWear), 1);
            TyreNoWearWearLimit = 100 * (1 - wheelInfo.TyreWear.NoWearWearLimit);
            TyreMildWearLimit = 100 * (1 - wheelInfo.TyreWear.LightWearLimit);
            TyreHeavyWearLimit = 100 * (1 - wheelInfo.TyreWear.HeavyWearLimit);

            if (wheelInfo.TyreCoreTemperature.ActualQuantity.InCelsius > -200 && (TyreCoreTemperature?.Equals(wheelInfo.TyreCoreTemperature, TemperatureTolerance) != true))
            {
                TyreCoreTemperature = wheelInfo.TyreCoreTemperature;
            }

            if (TyreLeftTemperature?.Equals(wheelInfo.LeftTyreTemp, TemperatureTolerance) != true)
            {
                TyreLeftTemperature = wheelInfo.LeftTyreTemp;
            }

            if (wheelInfo.CenterTyreTemp.ActualQuantity.InCelsius > -200 && (TyreCenterTemperature?.Equals(wheelInfo.CenterTyreTemp, TemperatureTolerance) != true))
            {
                TyreCenterTemperature = wheelInfo.CenterTyreTemp;
            }

            if (TyreRightTemperature?.Equals(wheelInfo.RightTyreTemp, TemperatureTolerance) != true)
            {
                TyreRightTemperature = wheelInfo.RightTyreTemp;
            }

            if (TyrePressure?.Equals(wheelInfo.TyrePressure, PressureTolerance) != true)
            {
                TyrePressure = wheelInfo.TyrePressure;
            }

            if (BrakeTemperature?.Equals(wheelInfo.BrakeTemperature, TemperatureTolerance) != true)
            {
                BrakeTemperature = wheelInfo.BrakeTemperature;
            }

            UpdateCompound(wheelInfo);
            IdealPressure = "Ideal: " + wheelInfo.TyrePressure.IdealQuantity.GetValueInUnits(PressureUnits).ToStringScalableDecimals();
            IsTyreDetached = wheelInfo.Detached;
            BrakeCondition = Math.Round(100 * (1 - wheelInfo.BrakesDamage.Damage), 0);
            IsCamberVisible = _settingsProvider.DisplaySettingsViewModel.EnableCamberVisualization;

            if (IsCamberVisible)
            {
                WheelCamber = Math.Round(wheelInfo.Camber.GetValueInUnits(AngleUnits.Degrees), 1);
            }
            else
            {
                WheelCamber = 0;
            }
        }

        private void UpdateCompound(WheelInfo wheelInfo)
        {
            TyreCompound = wheelInfo.TyreCompoundWithSet;
        }

        public void UpdateSlippingInformation(SimulatorDataSet dataSet, WheelInfo wheelInfo, double radiusInMeters)
        {
            if (dataSet?.PlayerInfo == null || wheelInfo == null)
            {
                SlipStatus = WheelSlipStatus.NotSlipping;
                return;
            }

            if (!dataSet.SimulatorSourceInfo.TelemetryInfo.ContainsWheelRps || dataSet.PlayerInfo.Speed.InKph < 30)
            {
                SlipStatus = WheelSlipStatus.NotSlipping;
                return;
            }

            double wheelVelocity = wheelInfo.Rps * radiusInMeters;
            bool isSLipping = wheelVelocity < dataSet.PlayerInfo.Speed.InMs * 0.7;
            if (isSLipping)
            {
                SlipStatus = WheelSlipStatus.IsSlipping;
                return;
            }

            switch (SlipStatus)
            {
                case WheelSlipStatus.IsSlipping:
                    SlipStatus = WheelSlipStatus.WasSlipping;
                    _slipEndSessionTime = dataSet.SessionInfo.SessionTime;
                    break;
                case WheelSlipStatus.WasSlipping when (dataSet.SessionInfo.SessionTime - _slipEndSessionTime).TotalSeconds > 10:
                    SlipStatus = WheelSlipStatus.NotSlipping;
                    break;
            }
        }

        public void ApplyWheelCondition(SimulatorDataSet dataSet, WheelInfo wheelInfo)
        {
            if (dataSet.PlayerInfo == null)
            {
                return;
            }

            if (dataSet.SessionInfo.SessionType != SessionType.Race && dataSet.PlayerInfo.InPits)
            {
                UpdateCompound(wheelInfo);
                return;
            }

            ApplyWheelCondition(wheelInfo);

            if (_tyreLifeTimeMonitor == null)
            {
                return;
            }

            _tyreLifeTimeMonitor.ApplyWheelInfo(dataSet, wheelInfo);
            WearAtRaceEnd = Math.Round(_tyreLifeTimeMonitor.WearAtRaceEnd);
            WearAtStintEnd = Math.Round(_tyreLifeTimeMonitor.WearAtStintEnd);
            LapsUntilHeavyWear = _tyreLifeTimeMonitor.LapsUntilHeavyWear;
        }

        public void Reset()
        {
            _tyreLifeTimeMonitor?.Reset();
            SlipStatus = WheelSlipStatus.NotSlipping;
            _slipEndSessionTime = TimeSpan.Zero;
        }

        private void DisplaySettingsViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ApplyDisplaySettings();
        }

        private void ApplyDisplaySettings()
        {
            PressureUnits = _settingsProvider.DisplaySettingsViewModel.DisplayPressureUnits;
            TemperatureUnits = _settingsProvider.DisplaySettingsViewModel.TemperatureUnits;
        }
    }
}