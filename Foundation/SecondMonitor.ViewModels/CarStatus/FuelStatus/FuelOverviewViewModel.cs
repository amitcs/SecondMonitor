﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    using Contracts.Commands;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using Factory;
    using NLog;

    using SecondMonitor.ViewModels.Text;

    using Settings;
    using Settings.ViewModel;

    public class FuelOverviewViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {
        public const string ViewModelLayoutName = "Fuel Overview";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Stopwatch _refreshWatch;
        private readonly Stopwatch _informationRefreshWatch;
        private readonly SessionRemainingCalculator _sessionRemainingCalculator;

        private TimeSpan _timeLeft;
        private double _lapsLeft;
        private Volume _avgPerLap;
        private Volume _fuelLeft;
        private Volume _avgPerMinute;
        private Volume _currentPerLap;
        private Volume _currentPerMinute;
        private double _fuelPercentage;
        private FuelLevelStatus _fuelLevelState;
        private Volume _maximumFuel;
        private bool _showDeltaInfo;
        private TimeSpan _timeDelta;
        private double _lapsDelta;
        private Volume _fuelDelta;
        private bool _isVisible;
        private bool _isFuelCalculatorButtonEnabled;

        private ICommand _showFuelCalculatorCommand;
        private ICommand _hideFuelCalculatorCommand;
        private bool _isFuelToAddVisible;
        private string _fuelToAddLabel;
        private Volume _fuelToAdd;
        private bool _isPitStopCountVisible;
        private int _pitStopCount;
        private RefuelingWindowState _refuelingWindowState;
        private bool _isNextPitStopInfoVisible;
        private string _nextPitStopInfo;
        private RefuelingWindowState _nextPitStopState;
        private FontStyle _nextPitStopFontStyle;

        public FuelOverviewViewModel(SessionRemainingCalculator sessionRemainingCalculator, ISettingsProvider settingsProvider, IViewModelFactory viewModelFactory)
        {
            FuelPlannerViewModel = viewModelFactory.Create<FuelPlannerViewModel>();
            _refreshWatch = Stopwatch.StartNew();
            _informationRefreshWatch = Stopwatch.StartNew();
            _sessionRemainingCalculator = sessionRemainingCalculator;
            FuelConsumptionMonitor = new FuelConsumptionMonitor();
            ResetCommand = new RelayCommand(Reset);
            DisplaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
        }

        public FuelPlannerViewModel FuelPlannerViewModel { get; }

        public FontStyle NextPitStopFontStyle
        {
            get => _nextPitStopFontStyle;
            set => SetProperty(ref _nextPitStopFontStyle, value);
        }

        public bool IsNextPitStopInfoVisible
        {
            get => _isNextPitStopInfoVisible;
            set => SetProperty(ref _isNextPitStopInfoVisible, value);
        }

        public string NextPitStopInfo
        {
            get => _nextPitStopInfo;
            set => SetProperty(ref _nextPitStopInfo, value);
        }

        public RefuelingWindowState NextPitStopState
        {
            get => _nextPitStopState;
            set => SetProperty(ref _nextPitStopState, value);
        }

        public bool IsFuelToAddVisible
        {
            get => _isFuelToAddVisible;
            set => SetProperty(ref _isFuelToAddVisible, value);
        }

        public string FuelToAddLabel
        {
            get => _fuelToAddLabel;
            set => SetProperty(ref _fuelToAddLabel, value);
        }

        public Volume FuelToAdd
        {
            get => _fuelToAdd;
            set => SetProperty(ref _fuelToAdd, value);
        }

        public bool IsPitStopCountVisible
        {
            get => _isPitStopCountVisible;
            set => SetProperty(ref _isPitStopCountVisible, value);
        }

        public int PitStopCount
        {
            get => _pitStopCount;
            set => SetProperty(ref _pitStopCount, value);
        }

        public DisplaySettingsViewModel DisplaySettingsViewModel { get; }

        public FuelConsumptionMonitor FuelConsumptionMonitor { get; }

        public bool IsWetSession { get; private set; }

        public bool IsFuelCalculatorButtonEnabled
        {
            get => _isFuelCalculatorButtonEnabled;
            set => SetProperty(ref _isFuelCalculatorButtonEnabled, value);
        }

        public ICommand ResetCommand { get; }

        public ICommand ShowFuelCalculatorCommand
        {
            get => _showFuelCalculatorCommand;
            set => SetProperty(ref _showFuelCalculatorCommand, value);
        }

        public ICommand HideFuelCalculatorCommand
        {
            get => _hideFuelCalculatorCommand;
            set => SetProperty(ref _hideFuelCalculatorCommand, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public TimeSpan TimeDelta
        {
            get => _timeDelta;
            private set => SetProperty(ref _timeDelta, value);
        }

        public double LapsDelta
        {
            get => _lapsDelta;
            private set => SetProperty(ref _lapsDelta, value);
        }

        public Volume FuelDelta
        {
            get => _fuelDelta;
            private set => SetProperty(ref _fuelDelta, value);
        }

        public bool ShowDeltaInfo
        {
            get => _showDeltaInfo;
            private set => SetProperty(ref _showDeltaInfo, value);
        }

        public TimeSpan TimeLeft
        {
            get => _timeLeft;
            private set => SetProperty(ref _timeLeft, value);
        }

        public double LapsLeft
        {
            get => _lapsLeft;
            private set => SetProperty(ref _lapsLeft, value);
        }

        public Volume AvgPerLap
        {
            get => _avgPerLap;
            private set => SetProperty(ref _avgPerLap, value);
        }

        public Volume AvgPerMinute
        {
            get => _avgPerMinute;
            private set => SetProperty(ref _avgPerMinute, value);
        }

        public Volume CurrentPerLap
        {
            get => _currentPerLap;
            private set => SetProperty(ref _currentPerLap, value);
        }

        public Volume CurrentPerMinute
        {
            get => _currentPerMinute;
            private set => SetProperty(ref _currentPerMinute, value);
        }

        public double FuelPercentage
        {
            get => _fuelPercentage;
            private set => SetProperty(ref _fuelPercentage, value);
        }

        public FuelLevelStatus FuelState
        {
            get => _fuelLevelState;
            private set => SetProperty(ref _fuelLevelState, value);
        }

        public Volume MaximumFuel
        {
            get => _maximumFuel;
            private set => SetProperty(ref _maximumFuel, value);
        }

        public RefuelingWindowState RefuelingWindowState
        {
            get => _refuelingWindowState;
            private set => SetProperty(ref _refuelingWindowState, value);
        }

        private void ReApplyFuelLevels(FuelInfo fuel)
        {
            if (MaximumFuel != fuel.FuelCapacity)
            {
                MaximumFuel = fuel.FuelCapacity;
            }

            _fuelLeft = fuel.FuelRemaining;
            double fuelPercentage = Math.Round((fuel.FuelRemaining.InLiters / MaximumFuel.InLiters) * 100, 1);
            FuelPercentage = double.IsNaN(fuelPercentage) || double.IsInfinity(fuelPercentage) ? 0 : fuelPercentage;
        }

        private void UpdateActualData(SimulatorDataSet dataSet)
        {
            if (_informationRefreshWatch.ElapsedMilliseconds < 1000)
            {
                return;
            }

            CurrentPerLap = FuelConsumptionMonitor.ActPerLap;
            CurrentPerMinute = FuelConsumptionMonitor.ActPerMinute;
            AvgPerLap = FuelConsumptionMonitor.TotalPerLap;
            AvgPerMinute = FuelConsumptionMonitor.TotalPerMinute;

            if (AvgPerLap.InLiters > 0 && AvgPerMinute.InLiters > 0)
            {
                LapsLeft = _fuelLeft.InLiters / AvgPerLap.InLiters;
                TimeLeft = TimeSpan.FromMinutes(_fuelLeft.InLiters / AvgPerMinute.InLiters);
                UpdateFuelState(dataSet);
                UpdatePitStopEstimations(dataSet);
                UpdateNextPitStopInfo(dataSet);
            }

            _informationRefreshWatch.Restart();
        }

        private void UpdateNextPitStopInfo(SimulatorDataSet dataSet)
        {
            if (dataSet.SessionInfo.SessionType != SessionType.Race
                || FuelConsumptionMonitor.LapStartFuelStatus == null
                || FuelDelta.InLiters >= 0
                || TimeDelta.TotalSeconds > 0
                || LapsDelta > 0
                || dataSet.PlayerInfo == null
                || FuelConsumptionMonitor.LapStartFuelStatus.FuelLevel.InLiters < dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining.InLiters)
            {
                IsNextPitStopInfoVisible = false;
                NextPitStopState = RefuelingWindowState.Closed;
                NextPitStopInfo = string.Empty;
                return;
            }

            double fuelAtEndOfLap = FuelConsumptionMonitor.LapStartFuelStatus.FuelLevel.InLiters - FuelConsumptionMonitor.ActPerLap.InLiters - FuelConsumptionMonitor.ActPerMinute.InLiters * 0.333; //keep 20 seconds of reserve fuel
            int lapsOfFuelLeft = (int)((fuelAtEndOfLap / FuelConsumptionMonitor.TotalPerLap.InLiters));
            switch (lapsOfFuelLeft)
            {
                case 0:
                    NextPitStopInfo = "PIT THIS LAP";
                    NextPitStopState = dataSet.PlayerInfo.PitStopRequested || dataSet.PlayerInfo.InPits ? RefuelingWindowState.WithContingency : RefuelingWindowState.NoContingency;
                    NextPitStopFontStyle = FontStyle.Bold;
                    break;
                case 1:
                    NextPitStopInfo = "Pit Next Lap";
                    NextPitStopState = RefuelingWindowState.WithContingency;
                    NextPitStopFontStyle = FontStyle.Bold;
                    break;
                default:
                    NextPitStopInfo = $"Pit in {lapsOfFuelLeft + 1} laps";
                    NextPitStopState = RefuelingWindowState.Closed;
                    NextPitStopFontStyle = FontStyle.Normal;
                    break;
            }

            IsNextPitStopInfoVisible = true;
        }

        private void UpdatePitStopEstimations(SimulatorDataSet dataSet)
        {
            if (dataSet.SessionInfo.SessionType != SessionType.Race || FuelDelta.InLiters >= 0 || TimeDelta.TotalSeconds > 0 || LapsDelta > 0)
            {
                IsFuelToAddVisible = false;
                IsPitStopCountVisible = false;
                RefuelingWindowState = RefuelingWindowState.Closed;
                return;
            }

            //It is impossible to use the full tank during a stint, as you need some fuel left to get the pit box
            double stintFuelCapacity = dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelCapacity.InLiters - AvgPerLap.InLiters;
            double fuelDelta = -FuelDelta.InLiters;
            int pitStopCount = (int)Math.Ceiling(fuelDelta / stintFuelCapacity);
            IsPitStopCountVisible = pitStopCount > 1;
            PitStopCount = pitStopCount;

            double fuelForLastPitStopInLiters = (fuelDelta) - ((pitStopCount - 1) * stintFuelCapacity);
            double fuelToAddWithoutContingency = fuelForLastPitStopInLiters;
            fuelForLastPitStopInLiters += AvgPerLap.InLiters * DisplaySettingsViewModel.ExtraRaceLaps;
            fuelForLastPitStopInLiters += AvgPerMinute.InLiters * DisplaySettingsViewModel.ExtraRaceMinutes;

            fuelForLastPitStopInLiters *= 1 + (DisplaySettingsViewModel.ExtraContingencyFuel / 100.0);

            FuelToAddLabel = PitStopCount > 1 ? "Last Stop: +" : "Add: ";

            IsFuelToAddVisible = true;
            FuelToAdd = Volume.FromLiters(Math.Ceiling(fuelForLastPitStopInLiters));

            if (dataSet.SessionInfo.PitWindow.PitWindowState != PitWindowState.None)
            {
                RefuelingWindowState = dataSet.SessionInfo.PitWindow.PitWindowState == PitWindowState.InPitWindow ? RefuelingWindowState.WithContingency : RefuelingWindowState.Closed;
            }
            else if (PitStopCount > 1)
            {
                RefuelingWindowState = dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining.InLiters <= AvgPerLap.InLiters ? RefuelingWindowState.WithContingency : RefuelingWindowState.Closed;
            }
            else
            {
                if (dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining.InLiters + FuelToAdd.InLiters < dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelCapacity.InLiters)
                {
                    RefuelingWindowState = RefuelingWindowState.WithContingency;
                }
                else if (dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining.InLiters + fuelToAddWithoutContingency < dataSet.PlayerInfo.CarInfo.FuelSystemInfo.FuelCapacity.InLiters)
                {
                    RefuelingWindowState = RefuelingWindowState.NoContingency;
                }
            }
        }

        private void UpdateFuelState(SimulatorDataSet dataSet)
        {
            switch (dataSet.SessionInfo.SessionType)
            {
                case SessionType.Qualification:
                    FuelState = FuelLevelStatus.Unknown;
                    break;
                case SessionType.Race:
                    UpdateFuelStateByLapsSessionLength(dataSet);
                    break;
                default:
                    UpdateFuelStateByLapsLeft();
                    break;
            }
        }

        private void UpdateFuelStateByLapsSessionLength(SimulatorDataSet dataSet)
        {
            if (dataSet.LeaderInfo == null)
            {
                return;
            }

            double lapsToGo = _sessionRemainingCalculator.GetLapsRemaining(dataSet);
            if (double.IsNaN(lapsToGo) || double.IsInfinity(lapsToGo))
            {
                return;
            }

            LapsDelta = LapsLeft - lapsToGo;
            FuelDelta = Volume.FromLiters(AvgPerLap.InLiters * LapsDelta);
            TimeDelta = TimeSpan.FromMinutes(FuelDelta.InLiters / AvgPerMinute.InLiters);
            if (LapsDelta > 1.5)
            {
                FuelState = FuelLevelStatus.IsEnoughForSession;
                return;
            }

            if (LapsDelta > 0)
            {
                FuelState = FuelLevelStatus.PossiblyEnoughForSession;
                return;
            }

            if (LapsLeft < 2)
            {
                FuelState = FuelLevelStatus.Critical;
            }
            else
            {
                FuelState = FuelLevelStatus.NotEnoughForSession;
            }
        }

        private void UpdateFuelStateByLapsLeft()
        {
            if (LapsLeft < 2)
            {
                FuelState = FuelLevelStatus.Critical;
                return;
            }

            if (LapsLeft < 4)
            {
                FuelState = FuelLevelStatus.NotEnoughForSession;
                return;
            }

            if (LapsLeft < 8)
            {
                FuelState = FuelLevelStatus.PossiblyEnoughForSession;
                return;
            }

            FuelState = FuelLevelStatus.IsEnoughForSession;
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            try
            {
                if (_refreshWatch.ElapsedMilliseconds < 500 || dataSet.SessionInfo.SessionType == SessionType.Na)
                {
                    return;
                }

                IsFuelCalculatorButtonEnabled = dataSet.PlayerInfo != null;

                if (dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown || dataSet.PlayerInfo == null)
                {
                    return;
                }

                _refreshWatch.Restart();
                if (!IsWetSession && (dataSet.SessionInfo.WeatherInfo.RainIntensity > 0 || dataSet.SessionInfo.WeatherInfo.TrackWetness > 0))
                {
                    IsWetSession = true;
                }

                ShowDeltaInfo = dataSet.SessionInfo.SessionType == SessionType.Race;
                ReApplyFuelLevels(dataSet.PlayerInfo.CarInfo.FuelSystemInfo);
                FuelConsumptionMonitor.UpdateFuelConsumption(dataSet);
                UpdateActualData(dataSet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void Reset()
        {
            RefuelingWindowState = RefuelingWindowState.Closed;
            IsPitStopCountVisible = false;
            PitStopCount = 0;
            IsFuelToAddVisible = false;
            FuelToAdd = Volume.FromLiters(0);
            FuelToAddLabel = string.Empty;
            IsWetSession = false;
            FuelConsumptionMonitor.Reset();
            _sessionRemainingCalculator.Reset();
            CurrentPerLap = FuelConsumptionMonitor.ActPerLap;
            CurrentPerMinute = FuelConsumptionMonitor.ActPerMinute;
            AvgPerLap = FuelConsumptionMonitor.TotalPerLap;
            AvgPerMinute = FuelConsumptionMonitor.TotalPerMinute;
            FuelState = FuelLevelStatus.Unknown;
            TimeDelta = TimeSpan.Zero;
            LapsDelta = 0;
            FuelDelta = Volume.FromLiters(0);
            ShowDeltaInfo = false;
            Logger.Info("Fuel Overview Reset");
        }
    }
}