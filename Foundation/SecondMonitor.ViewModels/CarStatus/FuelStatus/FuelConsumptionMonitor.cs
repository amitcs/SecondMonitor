﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using System.Diagnostics;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Summary;
    using NLog;

    public class FuelConsumptionMonitor
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly bool _logExtendedFuelInfo = true;
        private readonly Stopwatch _extendedLogStopwatch;
        
        private int _lastLapNumber;
        private FuelStatusSnapshot _lastMinuteFuelStatus;
        private FuelStatusSnapshot _lastTickFuelStatus;
        private TimeSpan _nextMinuteConsumptionUpdate;

        public FuelConsumptionMonitor()
        {
            _extendedLogStopwatch = Stopwatch.StartNew();
/*#if !DEBUG
            _logExtendedFuelInfo = false;
#endif*/
            Reset();
        }

        public Volume ActPerMinute
        {
            get;
            private set;
        }

        public Volume ActPerLap
        {
            get;
            private set;
        }

        public Volume TotalPerMinute
        {
            get;
            private set;
        }

        public Volume TotalPerLap
        {
            get;
            private set;
        }

        public FuelStatusSnapshot LapStartFuelStatus { get; private set; }

        public FuelConsumptionInfo TotalFuelConsumptionInfo { get; private set; }

        public void Reset()
        {
            _nextMinuteConsumptionUpdate = TimeSpan.Zero;
            _lastTickFuelStatus = null;
            _lastMinuteFuelStatus = null;
            LapStartFuelStatus = null;
            _lastLapNumber = -1;
            TotalFuelConsumptionInfo = new FuelConsumptionInfo();
            ActPerMinute = Volume.FromLiters(0);
            ActPerLap = Volume.FromLiters(0);
            TotalPerMinute = Volume.FromLiters(0);
            TotalPerLap = Volume.FromLiters(0);
        }

        private void UpdateMinuteConsumption(SimulatorDataSet simulatorDataSet)
        {
            if (_nextMinuteConsumptionUpdate > simulatorDataSet.SessionInfo.SessionTime)
            {
                return;
            }

            _nextMinuteConsumptionUpdate = simulatorDataSet.SessionInfo.SessionTime + TimeSpan.FromMinutes(1);

            if (_lastMinuteFuelStatus == null)
            {
                _lastMinuteFuelStatus = new FuelStatusSnapshot(simulatorDataSet);
                return;
            }

            FuelStatusSnapshot currentMinuteFuelConsumption = new FuelStatusSnapshot(simulatorDataSet);
            FuelConsumptionInfo fuelConsumption = FuelConsumptionInfo.CreateConsumption(_lastMinuteFuelStatus, currentMinuteFuelConsumption);
            ActPerMinute = fuelConsumption.ConsumedFuel;
            _lastMinuteFuelStatus = currentMinuteFuelConsumption;
        }

        private void UpdateLapConsumption(SimulatorDataSet simulatorDataSet)
        {
            if (_lastLapNumber == simulatorDataSet.PlayerInfo.CompletedLaps)
            {
                return;
            }

            _lastLapNumber = simulatorDataSet.PlayerInfo.CompletedLaps;

            if (LapStartFuelStatus == null)
            {
                LapStartFuelStatus = new FuelStatusSnapshot(simulatorDataSet);
                return;
            }

            FuelStatusSnapshot currentLapConsumption = new FuelStatusSnapshot(simulatorDataSet);
            FuelConsumptionInfo fuelConsumption = FuelConsumptionInfo.CreateConsumption(LapStartFuelStatus, currentLapConsumption);
            ActPerLap = fuelConsumption.ConsumedFuel;
            LapStartFuelStatus = currentLapConsumption;
        }

        public void UpdateFuelConsumption(SimulatorDataSet simulatorDataSet)
        {
            if (simulatorDataSet?.PlayerInfo == null)
            {
                return;
            }

            if (simulatorDataSet?.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining.InLiters <= 0)
            {
                return;
            }

            if (_lastTickFuelStatus != null && _lastTickFuelStatus.SessionTime >= simulatorDataSet.SessionInfo.SessionTime)
            {
                return;
            }

            if (SkipThisTick(simulatorDataSet))
            {
                // Force to also skip next tick
                _lastTickFuelStatus = null;
                return;
            }

            if (_lastTickFuelStatus == null)
            {
                _lastTickFuelStatus = new FuelStatusSnapshot(simulatorDataSet);
                LapStartFuelStatus = null;
                _lastMinuteFuelStatus = null;
                return;
            }

            UpdateLapConsumption(simulatorDataSet);
            UpdateMinuteConsumption(simulatorDataSet);

            FuelStatusSnapshot currentSnapshot = new FuelStatusSnapshot(simulatorDataSet);
            FuelConsumptionInfo lastTickConsumptionInfo = FuelConsumptionInfo.CreateConsumption(_lastTickFuelStatus, currentSnapshot);

            if (!lastTickConsumptionInfo.IsFuelConsumptionValid(simulatorDataSet))
            {
                _lastTickFuelStatus = currentSnapshot;
                return;
            }

            TotalFuelConsumptionInfo = TotalFuelConsumptionInfo.AddConsumption(lastTickConsumptionInfo);

            if (_logExtendedFuelInfo && _extendedLogStopwatch.Elapsed.TotalSeconds > 30)
            {
                Logger.Info("-------------------------Fuel Info Starts---------------------------");
                LogInfo(currentSnapshot);
                LogInfo(lastTickConsumptionInfo);
                LogInfo(TotalFuelConsumptionInfo);
                Logger.Info("-------------------------Fuel Info Ends---------------------------");
                _extendedLogStopwatch.Restart();
            }

            UpdateTotalData(simulatorDataSet);
            _lastTickFuelStatus = currentSnapshot;
        }

        private void LogInfo(FuelConsumptionInfo consumptionInfo)
        {
            Logger.Info($"Consumption Info: Elapsed Time (min) : {consumptionInfo.ElapsedTime.TotalMinutes:F2}, Total Distance: {consumptionInfo.TraveledDistance.InMeters:F2}, Total Consumtption: {consumptionInfo.ConsumedFuel.InLiters:F2}");
        }

        private void LogInfo(FuelStatusSnapshot currentSnapshot)
        {
            Logger.Info($"Fuel Snapshot: Session Time (min) : {currentSnapshot.SessionTime.TotalMinutes:F2}, Total Distance: {currentSnapshot.TotalDistance:F2}, Fuel Level: {currentSnapshot.FuelLevel.InLiters:F2}");
        }

        private void UpdateTotalData(SimulatorDataSet dataSet)
        {
            TotalPerMinute = TotalFuelConsumptionInfo.GetAveragePerMinute();
            TotalPerLap = TotalFuelConsumptionInfo.GetAveragePerDistance(dataSet.SessionInfo.TrackInfo.LayoutLength);
        }

        private bool SkipThisTick(SimulatorDataSet dataSet)
        {
            if (dataSet.PlayerInfo.InPits)
            {
                return true;
            }

            if (dataSet.SessionInfo.SessionType == SessionType.Race && (dataSet.SessionInfo.SessionPhase is SessionPhase.Countdown or SessionPhase.Unavailable || dataSet.PlayerInfo.TotalDistance < 300))
            {
                return true;
            }

            if (dataSet.SessionInfo.ActiveFlags.HasFlag(FlagKind.FullCourseYellow) || dataSet.SessionInfo.ActiveFlags.HasFlag(FlagKind.SafetyCar) || dataSet.SessionInfo.ActiveFlags.HasFlag(FlagKind.VirtualSafetyCar))
            {
                return true;
            }

            if (_lastTickFuelStatus?.SessionTime >= dataSet.SessionInfo.SessionTime)
            {
                return true;
            }

            return false;
        }
    }
}