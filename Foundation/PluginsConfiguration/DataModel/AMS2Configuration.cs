﻿namespace SecondMonitor.PluginsConfiguration.Common.DataModel
{
    public class Ams2Configuration
    {
        public bool SplitVintageTouringTier1 { get; set; } = true;

        public bool SplitVintageTouringTier2 { get; set; } = true;

        public bool SplitTsiCup { get; set; } = true;

        public bool SplitCarreraCup { get; set; } = true;

        public bool SplitLancerCup { get; set; } = true;

        public bool SplitGtClassics { get; set; } = true;
    }
}