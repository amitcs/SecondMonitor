﻿namespace SecondMonitor.WindowsControls.WPF.FuelControl
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;

    using DataModel.BasicProperties;
    using ViewModels.CarStatus.FuelStatus;

    public class FuelOverviewControl : Control, INotifyPropertyChanged
    {
        public static readonly DependencyProperty FuelPercentageProperty = DependencyProperty.Register(nameof(FuelPercentage), typeof(double), typeof(FuelOverviewControl), new PropertyMetadata() { PropertyChangedCallback = OnFuelLevelChanged });
        public static readonly DependencyProperty BlinkToColorProperty = DependencyProperty.Register(nameof(BlinkToColor), typeof(Color), typeof(FuelOverviewControl), new FrameworkPropertyMetadata { PropertyChangedCallback = OnColorChanged });
        public static readonly DependencyProperty FuelStatusColorProperty = DependencyProperty.Register(nameof(FuelStatusColor), typeof(SolidColorBrush), typeof(FuelOverviewControl));
        public static readonly DependencyProperty FuelStatusUnknownColorProperty = DependencyProperty.Register(nameof(FuelStatusUnknownColor), typeof(Color), typeof(FuelOverviewControl), new FrameworkPropertyMetadata { PropertyChangedCallback = OnColorChanged });
        public static readonly DependencyProperty FuelStatusOkColorProperty = DependencyProperty.Register(nameof(FuelStatusOkColor), typeof(Color), typeof(FuelOverviewControl), new FrameworkPropertyMetadata { PropertyChangedCallback = OnColorChanged });
        public static readonly DependencyProperty FuelStatusNotEnoughColorProperty = DependencyProperty.Register(nameof(FuelStatusNotEnoughColor), typeof(Color), typeof(FuelOverviewControl), new FrameworkPropertyMetadata { PropertyChangedCallback = OnColorChanged });
        public static readonly DependencyProperty FuelStatusMightBeColorProperty = DependencyProperty.Register(nameof(FuelStatusMightBeColor), typeof(Color), typeof(FuelOverviewControl), new FrameworkPropertyMetadata { PropertyChangedCallback = OnColorChanged });
        public static readonly DependencyProperty FuelStateProperty = DependencyProperty.Register(nameof(FuelState), typeof(FuelLevelStatus), typeof(FuelOverviewControl), new PropertyMetadata { PropertyChangedCallback = OnFuelStateChanged });
        public static readonly DependencyProperty MaximumFuelProperty = DependencyProperty.Register(nameof(MaximumFuel), typeof(Volume), typeof(FuelOverviewControl), new PropertyMetadata() { PropertyChangedCallback = OnFuelLevelChanged });
        public static readonly DependencyProperty TimeLeftProperty = DependencyProperty.Register(nameof(TimeLeft), typeof(TimeSpan), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty LapsLeftProperty = DependencyProperty.Register(nameof(LapsLeft), typeof(double), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty AvgPerLapProperty = DependencyProperty.Register(nameof(AvgPerLap), typeof(Volume), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty AvgPerMinuteProperty = DependencyProperty.Register(nameof(AvgPerMinute), typeof(Volume), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty CurPerLapProperty = DependencyProperty.Register(nameof(CurPerLap), typeof(Volume), typeof(FuelOverviewControl));
        public static readonly DependencyProperty CurPerMinuteProperty = DependencyProperty.Register(nameof(CurPerMinute), typeof(Volume), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty ResetCommandProperty = DependencyProperty.Register(nameof(ResetCommand), typeof(ICommand), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty VolumeUnitsProperty = DependencyProperty.Register(nameof(VolumeUnits), typeof(VolumeUnits), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty FuelCalculatorCommandProperty = DependencyProperty.Register(nameof(FuelCalculatorCommand), typeof(ICommand), typeof(FuelOverviewControl), new PropertyMetadata());
        public static readonly DependencyProperty ShowAdditionalInfoProperty = DependencyProperty.Register(nameof(ShowAdditionalInfo), typeof(bool), typeof(FuelOverviewControl));
        public static readonly DependencyProperty FuelDeltaProperty = DependencyProperty.Register(nameof(FuelDelta), typeof(Volume), typeof(FuelOverviewControl));
        public static readonly DependencyProperty LapsDeltaProperty = DependencyProperty.Register(nameof(LapsDelta), typeof(double), typeof(FuelOverviewControl));
        public static readonly DependencyProperty TimeDeltaProperty = DependencyProperty.Register(nameof(TimeDelta), typeof(TimeSpan), typeof(FuelOverviewControl));
        public static readonly DependencyProperty PitStopCountProperty = DependencyProperty.Register(nameof(PitStopCount), typeof(int), typeof(FuelOverviewControl));
        public static readonly DependencyProperty IsPitStopCountVisibleProperty = DependencyProperty.Register(nameof(IsPitStopCountVisible), typeof(bool), typeof(FuelOverviewControl));
        public static readonly DependencyProperty FuelToAddProperty = DependencyProperty.Register(nameof(FuelToAdd), typeof(Volume), typeof(FuelOverviewControl));
        public static readonly DependencyProperty FuelToAddLabelProperty = DependencyProperty.Register(nameof(FuelToAddLabel), typeof(string), typeof(FuelOverviewControl));
        public static readonly DependencyProperty IsFuelToAddVisibleProperty = DependencyProperty.Register(nameof(IsFuelToAddVisible), typeof(bool), typeof(FuelOverviewControl));
        public static readonly DependencyProperty RefuelingWindowStateProperty = DependencyProperty.Register(nameof(RefuelingWindowState), typeof(RefuelingWindowState), typeof(FuelOverviewControl));
        public static readonly DependencyProperty IsNextPitStopInfoVisibleProperty = DependencyProperty.Register(nameof(IsNextPitStopInfoVisible), typeof(bool), typeof(FuelOverviewControl), new PropertyMetadata(default(bool)));
        public static readonly DependencyProperty NextPitStopInfoProperty = DependencyProperty.Register(nameof(NextPitStopInfo), typeof(string), typeof(FuelOverviewControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty NextPitStopStateProperty = DependencyProperty.Register(nameof(NextPitStopState), typeof(RefuelingWindowState), typeof(FuelOverviewControl), new PropertyMetadata(default(RefuelingWindowState)));
        public static readonly DependencyProperty NextPitStopFontStyleProperty = DependencyProperty.Register(
            nameof(NextPitStopFontStyle), typeof(SecondMonitor.ViewModels.Text.FontStyle), typeof(FuelOverviewControl), new PropertyMetadata(default(SecondMonitor.ViewModels.Text.FontStyle)));

        private Storyboard _toOkStoryBoard;
        private Storyboard _toUnknownStoryBoard;
        private Storyboard _toPossibleEnoughStoryBoard;
        private Storyboard _toNotEnoughStoryBoard;
        private Storyboard _criticalStoryBoard;

        public event PropertyChangedEventHandler PropertyChanged;

        public SecondMonitor.ViewModels.Text.FontStyle NextPitStopFontStyle
        {
            get => (SecondMonitor.ViewModels.Text.FontStyle)GetValue(NextPitStopFontStyleProperty);
            set => SetValue(NextPitStopFontStyleProperty, value);
        }

        public RefuelingWindowState NextPitStopState
        {
            get => (RefuelingWindowState)GetValue(NextPitStopStateProperty);
            set => SetValue(NextPitStopStateProperty, value);
        }

        public string NextPitStopInfo
        {
            get => (string)GetValue(NextPitStopInfoProperty);
            set => SetValue(NextPitStopInfoProperty, value);
        }

        public bool IsNextPitStopInfoVisible
        {
            get => (bool)GetValue(IsNextPitStopInfoVisibleProperty);
            set => SetValue(IsNextPitStopInfoVisibleProperty, value);
        }

        public RefuelingWindowState RefuelingWindowState
        {
            get => (RefuelingWindowState)GetValue(RefuelingWindowStateProperty);
            set => SetValue(RefuelingWindowStateProperty, value);
        }

        public bool IsFuelToAddVisible
        {
            get => (bool)GetValue(IsFuelToAddVisibleProperty);
            set => SetValue(IsFuelToAddVisibleProperty, value);
        }

        public string FuelToAddLabel
        {
            get => (string)GetValue(FuelToAddLabelProperty);
            set => SetValue(FuelToAddLabelProperty, value);
        }

        public Volume FuelToAdd
        {
            get => (Volume)GetValue(FuelToAddProperty);
            set => SetValue(FuelToAddProperty, value);
        }

        public bool IsPitStopCountVisible
        {
            get => (bool)GetValue(IsPitStopCountVisibleProperty);
            set => SetValue(IsPitStopCountVisibleProperty, value);
        }

        public int PitStopCount
        {
            get => (int)GetValue(PitStopCountProperty);
            set => SetValue(PitStopCountProperty, value);
        }

        public TimeSpan TimeDelta
        {
            get => (TimeSpan)GetValue(TimeDeltaProperty);
            set => SetValue(TimeDeltaProperty, value);
        }

        public double LapsDelta
        {
            get => (double)GetValue(LapsDeltaProperty);
            set => SetValue(LapsDeltaProperty, value);
        }

        public Volume FuelDelta
        {
            get => (Volume)GetValue(FuelDeltaProperty);
            set => SetValue(FuelDeltaProperty, value);
        }

        public bool ShowAdditionalInfo
        {
            get => (bool)GetValue(ShowAdditionalInfoProperty);
            set => SetValue(ShowAdditionalInfoProperty, value);
        }

        //private bool _criticalStoryBoardStarted;

        public ICommand ResetCommand
        {
            get => (ICommand)GetValue(ResetCommandProperty);
            set => SetValue(ResetCommandProperty, value);
        }

        public ICommand FuelCalculatorCommand
        {
            get => (ICommand)GetValue(FuelCalculatorCommandProperty);
            set => SetValue(FuelCalculatorCommandProperty, value);
        }

        public TimeSpan TimeLeft
        {
            get => (TimeSpan)GetValue(TimeLeftProperty);
            set => SetValue(TimeLeftProperty, value);
        }

        public VolumeUnits VolumeUnits
        {
            get => (VolumeUnits)GetValue(VolumeUnitsProperty);
            set => SetValue(VolumeUnitsProperty, value);
        }

        public double LapsLeft
        {
            get => (double)GetValue(LapsLeftProperty);
            set => SetValue(LapsLeftProperty, value);
        }

        public Volume AvgPerLap
        {
            get => (Volume)GetValue(AvgPerLapProperty);
            set => SetValue(AvgPerLapProperty, value);
        }

        public Volume AvgPerMinute
        {
            get => (Volume)GetValue(AvgPerMinuteProperty);
            set => SetValue(AvgPerMinuteProperty, value);
        }

        public Volume CurPerLap
        {
            get => (Volume)GetValue(CurPerLapProperty);
            set => SetValue(CurPerLapProperty, value);
        }

        public Volume CurPerMinute
        {
            get => (Volume)GetValue(CurPerMinuteProperty);
            set => SetValue(CurPerMinuteProperty, value);
        }

        public double FuelPercentage
        {
            get => (double)GetValue(FuelPercentageProperty);
            set => SetValue(FuelPercentageProperty, value);
        }

        public SolidColorBrush FuelStatusColor
        {
            get => (SolidColorBrush)GetValue(FuelStatusColorProperty);
            set => SetValue(FuelStatusColorProperty, value);
        }

        public Color FuelStatusUnknownColor
        {
            get => (Color)GetValue(FuelStatusUnknownColorProperty);
            set => SetValue(FuelStatusUnknownColorProperty, value);
        }

        public FuelLevelStatus FuelState
        {
            get => (FuelLevelStatus)GetValue(FuelStateProperty);
            set => SetValue(FuelStateProperty, value);
        }

        public Color FuelStatusOkColor
        {
            get => (Color)GetValue(FuelStatusOkColorProperty);
            set => SetValue(FuelStatusOkColorProperty, value);
        }

        public Color BlinkToColor
        {
            get => (Color)GetValue(BlinkToColorProperty);
            set => SetValue(BlinkToColorProperty, value);
        }

        public Color FuelStatusNotEnoughColor
        {
            get => (Color)GetValue(FuelStatusNotEnoughColorProperty);
            set => SetValue(FuelStatusNotEnoughColorProperty, value);
        }

        public Color FuelStatusMightBeColor
        {
            get => (Color)GetValue(FuelStatusMightBeColorProperty);
            set => SetValue(FuelStatusMightBeColorProperty, value);
        }

        public Volume MaximumFuel
        {
            get => (Volume)GetValue(MaximumFuelProperty);
            set => SetValue(MaximumFuelProperty, value);
        }

        public Volume FuelLeft
        {
            get;
            private set;
        }

        private static void OnColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FuelOverviewControl fuelOverviewControl)
            {
                fuelOverviewControl.UpdateStoryBoards();
            }
        }

        private static void OnFuelStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FuelOverviewControl fuelOverviewControl)
            {
                fuelOverviewControl.StartProperStoryBoard();
            }
        }

        private static void OnFuelLevelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FuelOverviewControl fuelOverviewControl)
            {
                fuelOverviewControl.UpdateFuelLevels();
            }
        }

        private void StartProperStoryBoard()
        {
            if (_toOkStoryBoard == null || _toNotEnoughStoryBoard == null || _toPossibleEnoughStoryBoard == null
                || _toNotEnoughStoryBoard == null || _criticalStoryBoard == null)
            {
                return;
            }

            /*try
            {
                if (_criticalStoryBoardStarted && _criticalStoryBoard.GetCurrentState(this) == ClockState.Active)
                {
                    _criticalStoryBoard.Stop(this);
                    _criticalStoryBoardStarted = false;
                }
            }
            catch (InvalidOperationException)
            {

            }*/

            switch (FuelState)
            {
                case FuelLevelStatus.Unknown:
                    _toUnknownStoryBoard.Begin(this);
                    break;
                case FuelLevelStatus.IsEnoughForSession:
                    _toOkStoryBoard.Begin(this);
                    break;
                case FuelLevelStatus.PossiblyEnoughForSession:
                    _toPossibleEnoughStoryBoard.Begin(this);
                    break;
                case FuelLevelStatus.NotEnoughForSession:
                    /*_toNotEnoughStoryBoard.Begin(this);
                    break;*/
                case FuelLevelStatus.Critical:
                    _toNotEnoughStoryBoard.Begin(this);
                    /*_criticalStoryBoard.Begin(this);
                    _criticalStoryBoardStarted = true;*/
                    break;
            }
        }

        private void UpdateStoryBoards()
        {
            _toOkStoryBoard = PrepareStoryBoard(FuelStatusOkColor);
            _toUnknownStoryBoard = PrepareStoryBoard(FuelStatusUnknownColor);
            _toPossibleEnoughStoryBoard = PrepareStoryBoard(FuelStatusMightBeColor);
            _toNotEnoughStoryBoard = PrepareStoryBoard(FuelStatusNotEnoughColor);
            _criticalStoryBoard = PrepareCriticalStoryBoard();
        }

        private Storyboard PrepareStoryBoard(Color endColor)
        {
            ColorAnimation toColorAnimation = new ColorAnimation(endColor, TimeSpan.FromSeconds(1));
            Storyboard sb = new Storyboard();
            sb.Children.Add(toColorAnimation);
            Storyboard.SetTarget(toColorAnimation, this);
            Storyboard.SetTargetProperty(toColorAnimation, new PropertyPath("FuelStatusColor.Color"));
            return sb;
        }

        private Storyboard PrepareCriticalStoryBoard()
        {
            ColorAnimation toRedAnimation = new ColorAnimation(FuelStatusNotEnoughColor, BlinkToColor, TimeSpan.FromSeconds(1));
            ColorAnimation backAnimation = new ColorAnimation(BlinkToColor, FuelStatusNotEnoughColor, TimeSpan.FromSeconds(1));
            backAnimation.BeginTime = TimeSpan.FromSeconds(1);
            Storyboard sb = new Storyboard();
            sb.Children.Add(toRedAnimation);
            sb.Children.Add(backAnimation);
            sb.Duration = TimeSpan.FromSeconds(2);
            Storyboard.SetTarget(toRedAnimation, this);
            Storyboard.SetTargetProperty(toRedAnimation, new PropertyPath("FuelStatusColor.Color"));
            Storyboard.SetTarget(backAnimation, this);
            Storyboard.SetTargetProperty(backAnimation, new PropertyPath("FuelStatusColor.Color"));
            sb.RepeatBehavior = RepeatBehavior.Forever;
            return sb;
        }

        private void UpdateFuelLevels()
        {
            if (MaximumFuel == null)
            {
                return;
            }

            Volume newValue = MaximumFuel * FuelPercentage / 100;
            if (FuelLeft != null && Math.Abs(newValue.InLiters - FuelLeft.InLiters) < 0.1)
            {
                return;
            }

            FuelLeft = newValue;
            NotifyPropertyChanged(nameof(FuelLeft));
        }

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}