﻿namespace SecondMonitor.WindowsControls.WPF.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    public class BoolToYellowGreenConverter : IValueConverter
    {
        private static readonly ResourceDictionary ColorResourceDictionary = new ResourceDictionary
        {
            Source = new Uri(@"pack://application:,,,/WindowsControls;component/WPF/Colors.xaml", UriKind.RelativeOrAbsolute)
        };

        private static readonly SolidColorBrush YellowBrush;
        private static readonly SolidColorBrush GreenBrush;

        static BoolToYellowGreenConverter()
        {
            YellowBrush = (SolidColorBrush)ColorResourceDictionary["Yellow01Brush"];
            GreenBrush = (SolidColorBrush)ColorResourceDictionary["Green01Brush"];
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool boolValue))
            {
                return Brushes.Transparent;
            }

            return boolValue ? GreenBrush : YellowBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}