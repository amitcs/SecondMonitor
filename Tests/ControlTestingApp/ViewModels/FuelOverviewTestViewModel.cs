﻿namespace ControlTestingApp.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using SecondMonitor.DataModel.BasicProperties;

    public class FuelOverviewTestViewModel : INotifyPropertyChanged
    {
        public FuelLevelStatus FuelLevelStatus
        {
            get;
            private set;
        }

        public Volume FuelLeft
        {
            get;
            private set;
        }

        public TimeSpan TimeLeft => TimeSpan.FromMinutes(25);

        public Volume CurrentFuel => Volume.FromLiters(15);

        public Volume MaxFuel => Volume.FromLiters(45);

        public Volume AvgPerLap => Volume.FromLiters(2);

        public Volume FuelToAdd => Volume.FromLiters(12.5);

        public Volume AvgPerMinute => Volume.FromLiters(5);

        public double FuelLeftRaw
        {
            set
            {
                FuelLeft = Volume.FromLiters(value);
                OnPropertyChanged(nameof(FuelLeft));
            }
        }

        public double FuelStatusDouble
        {
            set
            {
                int fuelState = (int)value;
                FuelLevelStatus = (FuelLevelStatus)fuelState;
                OnPropertyChanged(nameof(FuelLevelStatus));
            }
        }

        public TimeSpan TimeDelta => TimeSpan.FromMinutes(5);

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}