﻿namespace ControlTestingApp.ViewModels
{
    using System.Collections.Generic;
    using Ninject;
    using SecondMonitor.Bootstrapping;
    using SecondMonitor.DataModel.Snapshot.Drivers;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Creation.Calendar.Predefined;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview;
    using SecondMonitor.Rating.Common.Championship.Calendar.Templates.CalendarGroups;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;
    using SecondMonitor.ViewModels.PitBoard;
    using SecondMonitor.ViewModels.StatusIcon;

    public class MainTestWindowViewModel : AbstractViewModel
    {
        public MainTestWindowViewModel()
        {
            var kernel = new StandardKernel();
            kernel.LoadCore();
            IViewModelFactory viewModelFactory = kernel.Get<IViewModelFactory>();

            CalendarTemplateGroupViewModel = viewModelFactory.Create<PredefinedCalendarSelectionViewModel>();
            CalendarTemplateGroupViewModel.FromModel(AllGroups.MainGroup);

            ClearToRejoinBoardViewModel = viewModelFactory.Create<ClearToRejoinBoardViewModel>();

            SafetyCarPitBoardViewModel = viewModelFactory.Create<SafetyCarPitBoardViewModel>();
            SafetyCarPitBoardViewModel.AdditionalText = "Ending";
            SafetyCarPitBoardViewModel.AdditionalTextState = StatusIconState.Ok;
            SafetyCarPitBoardViewModel.SafetyCarIconState = StatusIconState.Ok;

            YellowAheadPitBoardViewModel = viewModelFactory.Create<YellowAheadPitBoardViewModel>();
            YellowAheadPitBoardViewModel.SetSectorInformation("Sector 1", 
                new List<DriverWithClassModel>()
                {
                    new DriverWithClassModel(5, "Driver fafa", "Gt3", true),
                    new DriverWithClassModel(16, "Driver Fafa23", "Gt4", true),
                    new DriverWithClassModel(16, "Driver", "Gt3", true)
                });

            YellowAheadPitBoardViewModel.SetSectorInformation("Sector 1", 
                new List<DriverWithClassModel>()
                {
                    new DriverWithClassModel(16, "Driver Fafa23", "Gt4", true),
                });
            FuelOverviewTestViewModel = new FuelOverviewTestViewModel();

            List<IDriverInfo> driverInfos = new List<IDriverInfo>()
            {
                new DriverInfo()
                {
                    DriverShortName = "Driver 1",
                    DistanceToPlayer = 342,
                },
                new DriverInfo()
                {
                    DriverShortName = "Driver 2",
                    DistanceToPlayer = 342,
                },
                new DriverInfo()
                {
                    DriverShortName = "Driver 3",
                    DistanceToPlayer = 342,
                },

                new DriverInfo()
                {
                    DriverShortName = "Driver 4",
                    DistanceToPlayer = 342,
                },
                new DriverInfo()
                {
                    DriverShortName = "Driver 5",
                    DistanceToPlayer = 342,
                },
            };

            ClearToRejoinBoardViewModel.FromModel(driverInfos);

            SequenceViewTestViewModel = new SequenceViewTestViewModel();
            TrophyViewModel = new TrophyViewModel()
            {
                DriverName = "Fooo Foookovic",
                Position = 3,
            };
        }

        public PredefinedCalendarSelectionViewModel CalendarTemplateGroupViewModel { get; }

        public SequenceViewTestViewModel SequenceViewTestViewModel { get; }

        public ClearToRejoinBoardViewModel ClearToRejoinBoardViewModel { get; }

        public SafetyCarPitBoardViewModel SafetyCarPitBoardViewModel { get; }

        public YellowAheadPitBoardViewModel YellowAheadPitBoardViewModel { get; }

        public FuelOverviewTestViewModel FuelOverviewTestViewModel { get; }

        public TrophyViewModel TrophyViewModel { get; }
    }
}