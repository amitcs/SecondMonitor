﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Events
{
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class SessionResultWithTitleViewModel : AbstractViewModel<(ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto)>
    {
        private bool _showManufacturerResults;
        private string _remark;

        public SessionResultWithTitleViewModel(IViewModelFactory viewModelFactory)
        {
            EventTitleViewModel = viewModelFactory.Create<EventTitleViewModel>();
            DriversSessionResultViewModel = viewModelFactory.Create<DriversSessionResultViewModel>();
            ManufacturersSessionResultViewModel = viewModelFactory.Create<ManufacturersSessionResultViewModel>();
        }

        public EventTitleViewModel EventTitleViewModel { get; }
        public DriversSessionResultViewModel DriversSessionResultViewModel { get; }
        public ManufacturersSessionResultViewModel ManufacturersSessionResultViewModel { get; }
        public string Remark
        {
            get => _remark;
            private set => SetProperty(ref _remark, value);
        }

        public bool ShowManufacturerResults
        {
            get => _showManufacturerResults;
            private set => SetProperty(ref _showManufacturerResults, value);
        }

        protected override void ApplyModel((ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) model)
        {
            (ChampionshipDto championship, EventDto eventDto, SessionDto sessionDto) = model;
            EventTitleViewModel.FromModel(model);
            DriversSessionResultViewModel.FromModel(sessionDto.SessionResult);

            ShowManufacturerResults = championship.IsManufacturerScoringEnabled && championship.Manufacturers.Count > 0;
            ManufacturersSessionResultViewModel.FromModel(sessionDto.SessionResult);

            Remark = sessionDto.SessionResult.Remarks;
        }

        public override (ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) SaveToNewModel()
        {
            throw new System.NotSupportedException();
        }
    }
}