﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.DataModel.Championship;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class ManufacturersStandingOverviewViewModel : AbstractViewModel<IEnumerable<ManufacturerDto>>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public ManufacturersStandingOverviewViewModel(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            Header = "Current Manufacturers Standings";
        }

        public string Header { get; set; }

        public IReadOnlyCollection<ManufacturerStandingViewModel> ManufacturerStandings { get; private set; }

        protected override void ApplyModel(IEnumerable<ManufacturerDto> model)
        {
            int totalGap = 0;
            int previousPoints = 0;
            bool isFirst = true;
            ManufacturerStandings = model.OrderBy(x => x.Position).Select(x =>
            {
                ManufacturerStandingViewModel newViewModel = _viewModelFactory.Create<ManufacturerStandingViewModel>();
                newViewModel.FromModel(x);
                if (!isFirst)
                {
                    int gap = previousPoints - x.TotalPoints;
                    totalGap = gap + totalGap;
                    newViewModel.GapToPrevious = gap;
                    newViewModel.GapToLeader = totalGap;
                }

                previousPoints = x.TotalPoints;
                isFirst = false;
                return newViewModel;
            }).ToList();
        }

        public override IEnumerable<ManufacturerDto> SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}