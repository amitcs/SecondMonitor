﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Overview
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    using Common.DataModel.Championship;

    using Events;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class ChampionshipDetailViewModel : AbstractViewModel<ChampionshipDto>
    {
        private readonly DriversStandingOverviewViewModel _driversStandingOverviewViewModel;
        private readonly ManufacturersStandingOverviewViewModel _manufacturersStandingOverviewViewModel;
        private ICommand _closeCommand;

        public ChampionshipDetailViewModel(IViewModelFactory viewModelFactory)
        {
            StandingsViewModels = new List<IViewModel>();
            _driversStandingOverviewViewModel = viewModelFactory.Create<DriversStandingOverviewViewModel>();
            _manufacturersStandingOverviewViewModel = viewModelFactory.Create<ManufacturersStandingOverviewViewModel>();
            CalendarResultsOverview = viewModelFactory.Create<CalendarResultsOverviewViewModel>();
            ChampionshipSessionsResults = viewModelFactory.Create<ChampionshipSessionsResultsViewModel>();
        }

        public string ChampionshipName { get; private set; }

        public List<IViewModel> StandingsViewModels { get; }

        public CalendarResultsOverviewViewModel CalendarResultsOverview { get; }
        public ChampionshipSessionsResultsViewModel ChampionshipSessionsResults { get; }

        public ICommand CloseCommand
        {
            get => _closeCommand;
            set => SetProperty(ref _closeCommand, value);
        }

        protected override void ApplyModel(ChampionshipDto model)
        {
            if (model == null)
            {
                return;
            }

            StandingsViewModels.Clear();

            ChampionshipName = model.ChampionshipName;
            _driversStandingOverviewViewModel.FromModel(model.Drivers);
            CalendarResultsOverview.FromModel(model);
            ChampionshipSessionsResults.FromModel(model);

            StandingsViewModels.Add(_driversStandingOverviewViewModel);

            if (model.IsManufacturerScoringEnabled && model.Manufacturers.Count > 0)
            {
                _manufacturersStandingOverviewViewModel.FromModel(model.Manufacturers);
                StandingsViewModels.Add(_manufacturersStandingOverviewViewModel);
            }
        }

        public override ChampionshipDto SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}