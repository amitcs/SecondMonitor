﻿namespace SecondMonitor.Rating.Application.Championship.Operations
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;

    public class ManufacturerPositionComparer : IComparer<ManufacturerSessionResultDto>
    {
        private readonly List<ManufacturerSessionResultDto> _results;

        public ManufacturerPositionComparer(ChampionshipDto championshipDto, SessionResultDto currentSessionResult)
        {
            _results = championshipDto.GetAllResults().Concat(new SessionResultDto[] { currentSessionResult }).SelectMany(x => x.ManufacturersSessionResult).ToList();
        }

        public int Compare(ManufacturerSessionResultDto x, ManufacturerSessionResultDto y)
        {
            if (x == null && y == null)
            {
                return 0;
            }

            if (x == null || y == null)
            {
                return -1;
            }

            if (x.TotalPoints != y.TotalPoints)
            {
                //inverse comparison - more position occurrences means that the driver is "lower"
                return y.TotalPoints.CompareTo(x.TotalPoints);
            }

            for (int i = 1; i < 99; i++)
            {
                int driverXPositionCount = GetPositionsCount(i, x.ManufacturerName);
                int driverYPositionCount = GetPositionsCount(i, y.ManufacturerName);
                if (driverXPositionCount != driverYPositionCount)
                {
                    //inverse comparison - more position occurrences means that the driver is "lower"
                    return driverYPositionCount.CompareTo(driverXPositionCount);
                }
            }

            return 0;
        }

        private int GetPositionsCount(int position, string name)
        {
            return _results.Count(x => x.FinishPosition == position && x.ManufacturerName == name);
        }
    }
}