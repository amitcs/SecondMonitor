﻿namespace SecondMonitor.Rating.Application.Championship.Manufacturers
{
    using System;
    using System.Collections.Generic;

    public interface IManufacturerProvider
    {
        public bool TryGetManufacturer(string carName, out Manufacturer manufacturer);

        public ManufacturerMap<T> Map<T>(IEnumerable<T> entitiesToMap, Func<T, string> carNameGetter);
    }
}