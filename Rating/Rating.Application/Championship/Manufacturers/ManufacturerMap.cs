﻿namespace SecondMonitor.Rating.Application.Championship.Manufacturers
{
    using System.Collections.Generic;

    public class ManufacturerMap<T>
    {
        public ManufacturerMap(Dictionary<Manufacturer, List<T>> map, List<T> entitiesWithUnknownManufacturer)
        {
            Map = map;
            EntitiesWithUnknownManufacturer = entitiesWithUnknownManufacturer;
        }

        public Dictionary<Manufacturer, List<T>> Map { get; }

        public List<T> EntitiesWithUnknownManufacturer { get; }

        public int TotalManufacturers => Map.Keys.Count;
    }
}