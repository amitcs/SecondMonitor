﻿namespace SecondMonitor.Rating.Common.Factories
{
    using System.Linq;
    using DataModel;
    using SecondMonitor.DataModel.Summary;

    public class SessionFinishStateFactory : ISessionFinishStateFactory
    {
        public SessionFinishState Create(SessionSummary sessionSummary)
        {
            return new SessionFinishState(sessionSummary.TrackInfo.TrackFullName, sessionSummary.Drivers.Select(x => new DriverFinishState(x.DriverId, x.IsPlayer, x.DriverLongName, x.CarName, x.ClassName, x.FinishingPosition, x.AveragePosition)));
        }
    }
}