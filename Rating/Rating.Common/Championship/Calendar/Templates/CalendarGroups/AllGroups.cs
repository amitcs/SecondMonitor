﻿namespace SecondMonitor.Rating.Common.Championship.Calendar.Templates.CalendarGroups
{
    using System.Linq;

    public static class AllGroups
    {
        public static CalendarTemplateGroup MainGroup = new CalendarTemplateGroup("All", new[]
        {
            DtmGroup.AllDtmGroups,
            TCRGroups.AllTCRGroups,
            AudiTTGroup,
            AdacGTMasterCalendars.AllCalendars,
            DrmCalendars.AllCalendars,
            IndycarCalendars.AllIndyCar,
            Formula1Calendars.Formula1AllGroup,
            Formula2Calendars.Formula2AllGroup,
            Formula3Calendars.Formula3AllGroup,
            Formula4Calendars.Formula4AllGroup,
            ImsaCalendars.AllImsaCalendars,
            GT1Calendars.AllGt1Calendars,
            Gt2Calendars.AllGt2Calendars,
            Gt3Calendar.AllGt3Calendar,
            Gt4Calendar.AllGt4Calendar,
            WorldSportcarsCalendars.AllWorldSportcarsCalendars,
            WecCalendars.AllWecCalendars,
            PorscheSuperCupCalendars.AllPorscheSuperCupCalendars,
            V8SupercarsCalendar.AllV8SupercarsCalendars,
            M1ProCarCalendars.AllM1ProCarCalendars,
            FormulaRenaultCalendars.AllFormulaRenaultCalendars,
            BrazilianStockCarsCalendars.AllBrazilianStockCarsCalendars,
            FiaTruckRacing.AllFiaTruckRacing,
            BTCCCalendars.AllBTCCCalendars,
            PragaCalendars.AllPragaCalendars,
        }, Enumerable.Empty<CalendarTemplate>());

        public static CalendarTemplateGroup AudiTTGroup => new CalendarTemplateGroup("Audi TT Cup", new[] { AudiTTCalendars.AudiTT2016, AudiTTCalendars.AudiTT2017 });
    }
}