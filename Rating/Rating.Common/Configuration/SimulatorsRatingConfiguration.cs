﻿namespace SecondMonitor.Rating.Common.Configuration
{
    public class SimulatorsRatingConfiguration
    {
        public SimulatorRatingConfiguration[] SimulatorRatingConfigurations { get; set; }
    }
}