﻿namespace SecondMonitor.SimdataManagement.ViewModel
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;

    using DataModel.BasicProperties;
    using DataModel.OperationalRange;

    using ViewModels;
    using ViewModels.Factory;
    using ViewModels.Settings;

    public class CarModelPropertiesViewModel : AbstractViewModel<CarModelProperties>
    {
        private readonly ViewModelFactory _viewModelFactory;
        private double _minimalOptimalBrakeTemperature;
        private double _maximumOptimalBrakeTemperature;
        private Distance _frontWheelDiameter;
        private Distance _rearWheelDiameter;

        private string _carName;
        private int _wheelRotation;
        private bool _wasWheelDiameterSet;
        private ICommand _cancelCommand;
        private ICommand _okCommand;
        private ICommand _openTyreDiameterWizardCommand;
        private ICommand _resetCarSettingsCommand;
        private TyreCompoundPropertiesViewModel _selectedTyreCompound;

        public CarModelPropertiesViewModel(ViewModelFactory viewModelFactory, ISettingsProvider settingsProvider)
        {
            _viewModelFactory = viewModelFactory;
            TyreCompounds = new ObservableCollection<TyreCompoundPropertiesViewModel>();
            TemperatureUnit = settingsProvider.DisplaySettingsViewModel.TemperatureUnits;
            DistanceUnits = settingsProvider.DisplaySettingsViewModel.DistanceUnitsVerySmall;
            TemperatureUnitsSymbol = Temperature.GetUnitSymbol(TemperatureUnit);
        }

        public string TemperatureUnitsSymbol { get; }

        public string CarName
        {
            get => _carName;
            set
            {
                _carName = value;
                NotifyPropertyChanged();
            }
        }

        public int WheelRotation
        {
            get => _wheelRotation;
            set
            {
                _wheelRotation = value;
                NotifyPropertyChanged();
            }
        }

        public Distance FrontWheelDiameter
        {
            get => _frontWheelDiameter;
            set => SetProperty(ref _frontWheelDiameter, value);
        }

        public Distance RearWheelDiameter
        {
            get => _rearWheelDiameter;
            set => SetProperty(ref _rearWheelDiameter, value);
        }

        public double MinimalOptimalBrakeTemperature
        {
            get => _minimalOptimalBrakeTemperature;
            set => SetProperty(ref _minimalOptimalBrakeTemperature, value);
        }

        public double MaximumOptimalBrakeTemperature
        {
            get => _maximumOptimalBrakeTemperature;
            set => SetProperty(ref _maximumOptimalBrakeTemperature, value);
        }

        public ObservableCollection<TyreCompoundPropertiesViewModel> TyreCompounds { get; }

        public ICommand OkCommand
        {
            get => _okCommand;
            set => SetProperty(ref _okCommand, value);
        }

        public ICommand CancelCommand
        {
            get => _cancelCommand;
            set => SetProperty(ref _cancelCommand, value);
        }

        public TemperatureUnits TemperatureUnit { get; }

        public DistanceUnits DistanceUnits { get; }

        public ICommand OpenTyreDiameterWizardCommand
        {
            get => _openTyreDiameterWizardCommand;
            set => SetProperty(ref _openTyreDiameterWizardCommand, value);
        }

        public ICommand ResetCarSettingsCommand
        {
            get => _resetCarSettingsCommand;
            set => SetProperty(ref _resetCarSettingsCommand, value);
        }

        public TyreCompoundPropertiesViewModel SelectedTyreCompound
        {
            get => _selectedTyreCompound;
            set => SetProperty(ref _selectedTyreCompound, value);
        }

        protected override void ApplyModel(CarModelProperties model)
        {
            TyreCompounds.Clear();
            CarName = model.Name;
            WheelRotation = model.WheelRotation;
            FrontWheelDiameter = Distance.FromMeters(model.FrontWheelDiameter.InMeters);
            RearWheelDiameter = Distance.FromMeters(model.RearWheelDiameter.InMeters);
            MinimalOptimalBrakeTemperature = model.OptimalBrakeTemperature.GetValueInUnits(TemperatureUnit) - model.OptimalBrakeTemperatureWindow.GetValueInUnits(TemperatureUnit);
            MaximumOptimalBrakeTemperature = model.OptimalBrakeTemperature.GetValueInUnits(TemperatureUnit) + model.OptimalBrakeTemperatureWindow.GetValueInUnits(TemperatureUnit);
            _wasWheelDiameterSet = model.WasWheelDiameterSet;
            foreach (TyreCompoundProperties modelTyreCompoundsProperty in model.TyreCompoundsProperties)
            {
                TyreCompoundPropertiesViewModel newViewModel = _viewModelFactory.Create<TyreCompoundPropertiesViewModel>();
                newViewModel.FromModel(modelTyreCompoundsProperty);
                TyreCompounds.Add(newViewModel);
            }
        }

        public override CarModelProperties SaveToNewModel()
        {
            bool wasWheelDiameterChanged = OriginalModel.FrontWheelDiameter.InMillimeter != FrontWheelDiameter.InMillimeter ||
                                           OriginalModel.RearWheelDiameter.InMillimeter != RearWheelDiameter.InMillimeter;
            return new CarModelProperties()
            {
                Name = CarName,
                OptimalBrakeTemperature = Temperature.FrontUnits((MinimalOptimalBrakeTemperature + MaximumOptimalBrakeTemperature) * 0.5, TemperatureUnit),
                OptimalBrakeTemperatureWindow = Temperature.FrontUnits((MaximumOptimalBrakeTemperature - MinimalOptimalBrakeTemperature) * 0.5, TemperatureUnit),
                TyreCompoundsProperties = TyreCompounds.Select(x => x.SaveToNewModel()).ToList(),
                WheelRotation = WheelRotation,
                OriginalContainsOptimalTemperature = OriginalModel.OriginalContainsOptimalTemperature,
                FrontWheelDiameter = FrontWheelDiameter,
                RearWheelDiameter = RearWheelDiameter,
                WasWheelDiameterSet = _wasWheelDiameterSet || wasWheelDiameterChanged,
            };
        }
    }
}
