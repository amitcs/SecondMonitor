﻿namespace SecondMonitor.Timing.Application.PitStatistics
{
    using System;
    using System.Threading.Tasks;
    using Common.PitStatistics;
    using Common.SessionTiming.Drivers.Lap.PitStop;
    using Common.SessionTiming.Drivers.Lap.SectorTracker;
    using Contracts.Extensions;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using NLog;
    using ViewModels.Controllers;
    using ViewModels.SessionEvents;

    public class PitStopStatisticsController : IPitStopStatisticProvider, IController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IPitStopEvenProvider _pitStopEvenProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly object _lockObject;

        private PitStatistics _currentStatistics;

        public PitStopStatisticsController(IPitStopEvenProvider pitStopEvenProvider, ISessionEventProvider sessionEventProvider)
        {
            _pitStopEvenProvider = pitStopEvenProvider;
            _sessionEventProvider = sessionEventProvider;
            _lockObject = new object();
            Reset();
        }

        public PitStatistics GetCurrentStatisticsPlayerClass()
        {
            return _currentStatistics;
        }

        public void Reset()
        {
            _currentStatistics = new PitStatistics(TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero, 0);
            //_currentStatistics = new PitStatistics(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(8), 1);
        }

        public Task StartControllerAsync()
        {
            _pitStopEvenProvider.PitStopCompleted += PitStopEvenProviderOnPitStopCompleted;
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            _pitStopEvenProvider.PitStopCompleted -= PitStopEvenProviderOnPitStopCompleted;
            return Task.CompletedTask;
        }

        private async void PitStopEvenProviderOnPitStopCompleted(object sender, PitStopArgs e)
        {
            try
            {
                SimulatorDataSet simulatorDataSet = _sessionEventProvider.LastDataSet;
                DriverInfo player = simulatorDataSet.PlayerInfo;
                if (player == null || simulatorDataSet.SessionInfo.SessionType != SessionType.Race
                                   || simulatorDataSet.SessionInfo.SessionPhase != SessionPhase.Green
                                   || string.IsNullOrEmpty(player.CarClassId) || e.PitStop.Driver.CarClassId != player.CarClassId
                                   || e.PitStop.WasUnderFCY)
                {
                    return;
                }

                if (simulatorDataSet.SessionInfo.PitWindow.PitWindowState != PitWindowState.None && !e.PitStop.IsPitWindowStop && e.PitStop.WasDriveThrough)
                {
                    return;
                }

                await AddToStatistics(e.PitStop);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while creating pit statistics");
            }
        }

        private async Task AddToStatistics(PitStopInfo pitStopInfo)
        {
            if (!pitStopInfo.Driver.DriverLapSectorsTracker.TryGetSpecificTracker(out PitTimeLostTracker timeLostTracker))
            {
                Logger.Warn("Cannot find time lost tracker");
                return;
            }

            // some track, like AMS 2 ovals have weird pit exit, when sector tracking will not kick in until the car is back on the fast part of the oval
            // this can take +20seconds
            for (int i = 0; i < 20 && !timeLostTracker.IsLastPitStopTimeLostFilled; i++)
            {
                Logger.Info($"Last Pitstop time lost, for driver {pitStopInfo.Driver.DriverId}, not filled yet, wating");
                await Task.Delay(2000);
            }

            if (!timeLostTracker.IsLastPitStopTimeLostFilled)
            {
                Logger.Warn($"Last Pitstop time lost, for driver {pitStopInfo.Driver.DriverId}, not filled.");
                return;
            }

            TimeSpan timeLost = timeLostTracker.LastPitStopTimeLost;

            Logger.Info($"Pit Stop Statistics : Driver: {pitStopInfo.Driver.DriverId} Total {pitStopInfo.PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Pit Stall {pitStopInfo.PitStopStallDuration.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Pit Lane {pitStopInfo.PitStopLaneDuration.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Time Lost: {timeLost.FormatTimeSpanOnlySecondNoMiliseconds(false)}");
            if (timeLost < pitStopInfo.PitStopStallDuration)
            {
                Logger.Warn("Last Pitstop time lost was less than pit stall duration");
                return;
            }

            lock (_lockObject)
            {
                int newTotalEntries = _currentStatistics.EntriesCount + 1;

                TimeSpan newTotalAverage = TimeSpan.FromSeconds(((_currentStatistics.TotalPitDuration.TotalSeconds * _currentStatistics.EntriesCount) + pitStopInfo.PitStopDuration.TotalSeconds) / newTotalEntries);
                TimeSpan newPitStopStallAverage = TimeSpan.FromSeconds(((_currentStatistics.PitStallDuration.TotalSeconds * _currentStatistics.EntriesCount) + pitStopInfo.PitStopStallDuration.TotalSeconds) / newTotalEntries);
                TimeSpan newPitStopLaneAverage = TimeSpan.FromSeconds(((_currentStatistics.PitLaneDuration.TotalSeconds * _currentStatistics.EntriesCount) + pitStopInfo.PitStopLaneDuration.TotalSeconds) / newTotalEntries);
                TimeSpan newTimeLost = TimeSpan.FromSeconds(((_currentStatistics.TotalTimeLost.TotalSeconds * _currentStatistics.EntriesCount) + timeLost.TotalSeconds) / newTotalEntries);

                _currentStatistics = new PitStatistics(newTotalAverage, newPitStopStallAverage, newPitStopLaneAverage, timeLost, newTotalEntries);
                Logger.Info($"New Pit Stop Statistics : Total Average {newTotalAverage.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Pit Stall Average {newPitStopStallAverage.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Pit Lane Average {newPitStopLaneAverage.FormatTimeSpanOnlySecondNoMiliseconds(false)}, Total Time Lost: {newTimeLost.FormatTimeSpanOnlySecondNoMiliseconds(false)} Total Entries: {newTotalEntries}");
            }
        }
    }
}