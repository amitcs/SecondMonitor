﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap
{
    public enum LapInvalidationReasonKind
    {
        NoValidLapTime,
        CompletedDistanceLessThanLapThreshold,
        NotAllSectorHaveTime,
        SanityMaxSpeedViolated,
        InvalidatedBySectorTime,
        InvalidatedFirstLap,
        DriverDnf,
        DriverInPits,
        InvalidatedBySim
    }
}