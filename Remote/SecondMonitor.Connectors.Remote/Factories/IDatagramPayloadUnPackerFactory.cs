﻿namespace SecondMonitor.Connectors.Remote.Factories
{
    using SecondMonitor.Remote.Adapter;

    internal interface IDatagramPayloadUnPackerFactory
    {
        IDatagramPayloadUnPacker Create();
    }
}