﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("92fd224f-7ade-48c6-ba7e-d2eb7368e2d6")]

[assembly: InternalsVisibleTo("SecondMonitor.Remote.Tests")]
