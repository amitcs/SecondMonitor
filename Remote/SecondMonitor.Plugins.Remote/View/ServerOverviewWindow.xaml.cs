﻿namespace SecondMonitor.Plugins.Remote.View
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ServerOverviewWindow.xaml
    /// </summary>
    public partial class ServerOverviewWindow : Window
    {
        public ServerOverviewWindow()
        {
            InitializeComponent();
        }
    }
}
