namespace SecondMonitor.PCarsConnector.Enums
{
    public enum EApiStructLengths
    {
        StringLengthMax = 64,
        NumParticipants = 64
    }
}