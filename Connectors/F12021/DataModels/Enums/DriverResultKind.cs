﻿namespace SecondMonitor.F12021Connector.DataModels.Enums
{
    internal enum DriverResultKind
    {
        Na,
        Inactive,
        Active,
        Finished,
        Disqualified,
        NotClassified,
        Retired,
    }
}