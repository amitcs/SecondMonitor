﻿namespace SecondMonitor.TelemetryPresentation.OpenWindow
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for OpenWindow.xaml
    /// </summary>
    public partial class OpenWindow : Window
    {
        public OpenWindow()
        {
            InitializeComponent();
        }
    }
}
