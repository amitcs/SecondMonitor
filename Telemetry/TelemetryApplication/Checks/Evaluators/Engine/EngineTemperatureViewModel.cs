﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.Engine
{
    public class EngineTemperatureViewModel : AbstractEvaluatorViewModel
    {
        public EngineTemperatureViewModel() : base("Engine Temperature")
        {
        }
    }
}