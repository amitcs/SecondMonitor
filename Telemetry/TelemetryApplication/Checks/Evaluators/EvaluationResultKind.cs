﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators
{
    public enum EvaluationResultKind
    {
        None,
        Ok,
        Information,
        Warning,
        Error,
    }
}