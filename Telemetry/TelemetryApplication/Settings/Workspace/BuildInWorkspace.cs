﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Settings.Workspace
{
    public class BuildInWorkspace : Workspace
    {
        public override bool IsBuildIn => true;
    }
}